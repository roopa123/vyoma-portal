<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BranchTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

        public function testit_shows_the_login_form()
        {
            $response = $this->call('GET', 'login');

            $this->assertTrue($response->isOk());

        // Even though the two lines above may be enough,
        // you could also check for something like this:

            View::shouldReceive('make')->with('login');
        }

        public function testit_redirects_back_to_form_if_login_fails()
        {
            $credentials = [
                'email' => 'test@test.com',
                'password' => 'secret',
            ];

            Auth::shouldReceive('attempt')
                 ->once()
                 ->with($credentials)
                 ->andReturn(false);

            $this->call('POST', 'auth/login', $credentials);

            $this->assertRedirectedToAction(
                'AuthenticationController@login',
                null,
                ['flash_message']
            );
        }

    public function testit_redirects_to_home_page_after_user_logs_in()
    {
        $credentials = [
            'email' => 'roopa.j@compassitesinc.com',
            'password' => 'roopa123',
        ];

        Auth::shouldReceive('attempt')
             ->once()
             ->with($credentials)
             ->andReturn(true);

        $this->call('POST', 'auth/login', $credentials);

        $this->assertRedirectedTo('/');
    }

}
