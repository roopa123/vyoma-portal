<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Branch', function (Blueprint $table) {
            $table->increments('BranchId');
            $table->string('BranchName')->unique();
            $table->timestamps();
            $table->boolean('isDeleted','0');
            $table->char('status', 1, 'A');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Branch');

    }
}
