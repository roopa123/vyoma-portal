<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBayLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('BayLocation', function (Blueprint $table) {
            $table->char('BayLocationCode');
            $table->string('BayLocationDesc')->unique();
            $table->timestamps();
            $table->boolean('isDeleted','0');
            $table->char('status', 1, 'A');
            $table->primary('BayLocationCode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('BayLocation');
    }
}
