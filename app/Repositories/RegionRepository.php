<?php
namespace App\Repositories;

use App\Models\Branch;
use App\Models\Region;
use App\Models\User;
use DB;
use Carbon\Carbon;

class RegionRepository
{
    /**
    * Validation for assigning people to project and manager
    */
    public $validationRules = [
        'RegionName' => 'required|unique:Region|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/'
      ];

    // Update validation rule
    public function editValidationRules($region) {
             $rules = array('RegionName' => 'required|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]+$)+/|unique:Region,RegionName,'.$region['RegionId'].',RegionId');
             return $rules;
        }

    /*
    * Fetch all the Branch info details
    */
    /*public function getBranches($branchId)
    {
        $list = Branch::where('isDeleted', '0')->where('id', $branchId)->lists('branch_name', 'id');

        return $list;
    }*/

    /*
    * Fetch all the Branch info details
    */
    public function branchlists()
    {
        $list = Branch::where('status', 'A')->lists('BranchName', 'BranchId');

        return $list;
    }

    /*
    * Fetch all the Branch info details
    */
    public function branchlistWithId($branch)
    {
        $list = Branch::where('status', 'A')->where('BranchId', $branch)->lists('BranchName', 'BranchId');
        return $list;
    }

    /*
    * Insert row into Region table
    */
    public function insertData($inputData)
    {
    	$region_name = $inputData['RegionName'];
        $dataToInsert = array(
            'RegionName' => $region_name,
            'status' => 'A'
        );
        $inserted = DB::table('Region')->insert($dataToInsert);
        return $inserted;
    }

    /*
    * Getting region lists
    */
    public function regionlist()
    {
        $list = Region::where('status', 'A')->lists('RegionName', 'RegionId');

        return $list;
    }

    /*
    * Getting region lists with id
    */
    public function regionlistWithId($region)
    {
        $list = Region::where('status', 'A')->where('RegionId', $region)->lists('RegionName', 'RegionId');
        return $list;
    }

    /*
    * Getting region details with id
    */
    public function getRegionDetails($id)
    {
        $details = Region::where('RegionId', $id)->get();
        return $details;
    }

    /*
    * Update region details with id
    */
    public function updateRegionDetails($id)
    {

      DB::table('Region')->where('RegionId', $id)
            ->update(array('RegionName' => \Input::get('RegionName'),
                            'status' => \Input::get('status')));
    }

    /*
    * Autocomplete search for Region
    */
    public function autocompleteSearch($searchterm)
    {
         $term = $searchterm;

        $results = array();

        $queries = DB::table('Region')
            ->where('RegionName', 'LIKE', '%'.$term.'%')
            ->orWhere('status', 'LIKE', '%'.$term.'%')
            ->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();

        foreach ($queries as $query)
        {
            if(\Request::segment(3) == 'RegionName'){
                $results[] = [ 'id' => $query->RegionId, 'value' => $query->RegionName];
            }
            else if(\Request::segment(3) == 'status') {
                $results[] = [ 'id' => $query->RegionId, 'value' => $query->status];
            }
        }
        return \Response::json($results);
    }


    /*
    * Update region details with id
    */
    /*public function deleteRegionrow($regid)
    {
        $result = DB::table('Region')
            ->join('Station', 'Region.RegionId', '=', 'Station.RegionId')
            ->join('Bay', 'Station.StationId', '=', 'Bay.StationId')
            ->join('Host', 'Bay.BayId', '=', 'Host.HostId')
            ->where('Region.RegionId', $regid)
            ->update(array('Region.status' => 'D', 'Station.status' => 'D',
                            'Bay.status' => 'D',  'Host.status' => 'D'));
        if($result){
                return true;
        }
        else {
            $result = DB::table('Region')
            ->join('Station', 'Region.RegionId', '=', 'Station.RegionId')
            ->where('Region.RegionId', $regid)
            ->update(array('Region.status' => 'D',
                           'Station.status' => 'D'));
            if($result){
                return true;
            }
            else {
                $result = DB::table('Region')
                    ->where('Region.RegionId', $regid)
                    ->update(array('Region.status' => 'D'));
                if($result){
                    return true;
                }
            }
        }

    }*/
}
