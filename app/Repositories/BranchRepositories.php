<?php
namespace App\Repositories;

use App\Models\Branch;
use App\Models\Region;
use App\Models\Station;
use App\Models\Bay;
use App\Models\Host;
use App\Models\User;
use DB;
use Carbon\Carbon;

class BranchRepositories
{
    /**
     * Validation for assigning people to project and manager
     */
    public $validationRules = [
            'BranchName' => 'required|unique:Branch|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/',
            'status' => 'required'
        ];

     public function editValidationRules($branch) {
             $rules = array('BranchName' => 'required|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/|unique:Branch,BranchName,'.$branch['BranchId'].',BranchId');
             return $rules;
        }

    /*
    * Insert data to DB table
    */
    public function insertData($inputData)
    {
        $branch_name = $inputData['BranchName'];
        $dataToInsert = array(
          'BranchName' => $branch_name,
          'status' => $inputData['status']
        );
        $inserted = DB::table('Branch')->insert($dataToInsert);
        return $inserted;
    }

    /*
    * Fetch list of branches
    */
    public function branchlist()
    {
        $list = Branch::all();
        return $list;
    }

    /*
    * Count of Branches
    */
    public function branchCount(){
        $count = Branch::where('status','A')->count();

        return $count;
    }

    /*
    * Count of Regions
    */
    public function regionCount(){
        $count = Region::where('status','A')->count();

        return $count;
    }

    /*
    * Count of Stations
    */
    public function stationCount(){
        $count = Station::where('status','A')->count();

        return $count;
    }

    /*
    * Count of Bays
    */
    public function bayCount(){
        $count = Bay::where('status','A')->count();

        return $count;
    }

    /*
    * Count of Hosts
    */
    public function hostCount(){
        $count = Host::where('status','A')->count();

        return $count;
    }

    /*
    * Fetch Details of branch based on id
    */
    public function getBranchDetails($branchId)
    {
        $details = Branch::where('BranchId', $branchId)->get();
        return $details;
    }

    /*
    * Update Details of branch based on id
    */
    public function updateBranchDetails($branchId)
    {
       DB::table('Branch')->where('BranchId', $branchId)
            ->update(array('BranchName' => \Input::get('BranchName'),
                            'status' => \Input::get('status')));
    }

    /*
    * Autocomplete search for Branch
    */

    public function autocompleteSearch($searchterm)
    {
         $term = $searchterm;

        $results = array();

        $queries = DB::table('Branch')
            ->where('BranchName', 'LIKE', '%'.$term.'%')
            ->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();

        foreach ($queries as $query)
        {
            if(\Request::segment(3) == 'BranchName'){
                $results[] = [ 'id' => $query->BranchId, 'value' => $query->BranchName];
            }
            else if(\Request::segment(3) == 'status'){
                $results[] = [ 'id' => $query->BranchId, 'value' => $query->status];

            }
        }
        return \Response::json($results);
    }

    /*
    * Delete row of branch based on id
    */
   /* public function deleteBranch($branchId)
    {
        $result = DB::table('Branch')
            ->join('Station', 'Branch.BranchId', '=', 'Station.BranchId')
            ->join('Bay', 'Station.StationId', '=', 'Bay.StationId')
            ->join('Host', 'Bay.BayId', '=', 'Host.HostId')
            ->where('Branch.BranchId', $branchId)
            ->update(array('Branch.status' => 'D', 'Station.status' => 'D',
                            'Bay.status' => 'D',  'Host.status' => 'D'));
        if($result){
                return true;
        }
        else {
            $result = DB::table('Branch')
            ->join('Station', 'Branch.BranchId', '=', 'Station.BranchId')
            ->where('Branch.BranchId', $branchId)
            ->update(array('Branch.status' => 'D',
                           'Station.status' => 'D'));
            if($result){
                return true;
            }
            else {
                $result = DB::table('Branch')
                    ->where('Branch.BranchId', $branchId)
                    ->update(array('Branch.status' => 'D'));
                if($result){
                    return true;
                }
            }
        }
    }*/
}
