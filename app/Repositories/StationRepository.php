<?php
namespace App\Repositories;

use App\Models\Branch;
use App\Models\Region;
use App\Models\Station;
use App\Models\State;
use App\Models\User;
use DB;
use Carbon\Carbon;

class StationRepository
{
    /**
    * Validation for assigning people to project and manager
    */
    public $validationRules = [
            'StationName' => 'required|unique:Station|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/',
            'BranchName' => 'required',
            'RegionName' => 'required',
            'StateName' => 'required'
        ];

    // Update validation rule
    public function editValidationRules($station) {
             $rules = array('StationName' => 'required|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/|unique:Station,StationName,'.$station['StationId'].',StationId');
             return $rules;
        }

    /*
    * Get all the state details
    */
    public function statelist()
    {
        $list = State::lists('StateName', 'StateId');

        return $list;
    }

    /*
    * Get all the station details
    */
    public function stationlist()
    {
        $list = Station::where('status', 'A')->lists('StationName', 'StationId');

        return $list;
    }

    /*
    * Insert row details to DB table
    */
    public function insertData($inputData)
    {
        $station_name = $inputData['StationName'];
        $region_name = $inputData['RegionName'];
        $branch_name = $inputData['BranchName'];
        $state_name = $inputData['StateName'];
        $dataToInsert = array(
            'StationName' => $station_name,
            'RegionId' => $region_name,
            'BranchId' => $branch_name,
            'StateId' => $state_name,
            'status' => $inputData['Station_status']
        );
        $inserted = DB::table('Station')->insert($dataToInsert);
        return $inserted;
    }

    /*
    * Get region name based on id
    */
    public function getRegionName($id)
    {
        $details = Region::select('RegionName')
            ->where('RegionId', $id)->get();
        return $details;
    }

    /*
    * Get state name based on id
    */
    public function getStateName($id)
    {
        $details = State::select('StateName')
            ->where('StateId', $id)->get();
        return $details;
    }

    /*
    * Get Station details based on id
    */
    public function getStationDetails($id)
    {
        $details = Station::where('StationId', $id)->get();
        return $details;
    }

    /*
    * Update Station details based on id
    */
    public function updateStationData($id)
    {
        DB::table('Station')->where('StationId', $id)
            ->update(array('StationName' => \Input::get('StationName'),
                            'RegionId' => \Input::get('RegionName'),
                            'BranchId' => \Input::get('BranchName'),
                            'StateId' => \Input::get('StateName'),
                            'status' => \Input::get('Station_status')));
    }

    /*
    * Delete Station details based on id
    */
    /*public function deleteStationrow($stationid)
    {
        $result = DB::table('Station')
            ->join('Bay', 'Station.StationId', '=', 'Bay.StationId')
            ->join('Host', 'Bay.BayId', '=', 'Host.BayId')
            ->where('Station.StationId', $stationid)
            ->update(array( 'Station.status' => 'D',
                            'Bay.status' => 'D',  'Host.status' => 'D'));
        if($result){
                return true;
        }
        else {
            $result = DB::table('Station')
            ->where('Station.StationId', $stationid)
            ->update(array('Station.status' => 'D'));
            if($result){
                return true;
            }

        }
    }*/

    public function findRequestType($type, $Id) {
        if($type == 'branch' && $Id){
            $branch = Branch::where('BranchId', '=', $Id)->first();
            return $branch;
        } else if($type == 'region' && $Id) {
            $region = Region::where('RegionId', '=', $Id)->first();
            return $region;
        }
         else if($type == 'regionHome' && $Id) {
            $region = Region::where('RegionId', '=', $Id)->first();
            return $region;
        }
        return NULL;
    }

    public function autocompleteSearch($searchterm){
        $term = $searchterm;
        $colValue = \Request::segment(3);
        $results = array();

        $queries = DB::table('Station')
            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
            ->join('State', 'Station.StateId', '=', 'State.StateId');

            switch ($colValue) {
                case 'BranchName':
                    $queries->where('Branch.BranchName', 'LIKE', '%'.$term.'%');
                    break;
                case 'StationName':
                    $queries->where('StationName', 'LIKE', '%'.$term.'%');
                    break;
                case 'RegionName':
                    $queries->where('Region.RegionName', 'LIKE', '%'.$term.'%');
                    break;
                case 'StateName':
                    $queries->where('State.StateName', 'LIKE', '%'.$term.'%');
                    break;
                }
            $queries->groupBy(\Request::segment(3));
            $res = $queries->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();

        foreach ($res as $query)
        {
            if(\Request::segment(3) == 'StationName'){
                $results[] = [ 'id' => $query->StationId, 'value' => $query->StationName];
            }
            else if(\Request::segment(3) == 'BranchName') {
                $results[] = [ 'id' => $query->StationId, 'value' => $query->BranchName];
            }
            else if(\Request::segment(3) == 'RegionName'){
                $results[] = [ 'id' => $query->StationId, 'value' => $query->RegionName];
            }
            else if(\Request::segment(3) == 'StateName') {
                $results[] = [ 'id' => $query->StationId, 'value' => $query->StateName];
            }
        }
        return \Response::json($results);
    }

}
