<?php
namespace App\Repositories;

use App\Models\Branch;
use App\Models\Region;
use App\Models\Station;
use App\Models\State;
use App\Models\User;
use App\Models\Bay;
use Carbon\Carbon;
use DB;

class BayRepository
{
    /**
    * Validation for assigning people to project and manager
    */
    public $validationRules = [
            'StationName' => 'required',
            'BayLocationDesc' => 'required',
            'App' => 'required'
        ];
    /*
    * Insert data to DB table
    */
    public function insertData($inputData)
    {
        $station_name = $inputData['StationName'];
        $baylocation_name = $inputData['BayLocationDesc'];
        $app_name = $inputData['App'];
        $detailsBay = Bay::select('Bay.App')
                    ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                    ->where('Bay.BayLocationCode', $baylocation_name)
                    ->where('Bay.StationId', $station_name)
                    ->where('Bay.App', $inputData['App'])->first();
        if($detailsBay==null) {
            $dataToInsert = array(
            'StationId' => $station_name,
            'BayLocationCode' => $baylocation_name,
            'App' => $app_name,
            'status' => $inputData['Bay_status']
        );
        $inserted = DB::table('Bay')->insert($dataToInsert);
        return $inserted;

        }
        else {
            $error = 'Duplicate Entry';
            throw new \Exception($error);

        }

    }

    /*
    * Getting bay list
    */
    public function baylist()
    {
        $list = Bay::where('status', 'A')->lists('BayId');
        return $list;
    }

    /*
    * Station Info
    */
    public function getStationDetail($bayId)
    {
        $details = Bay::select('Station.StationName')
            ->where('Bay.BayId', $bayId)
            ->join('Station', 'Bay.StationId', '=', 'Station.StationId')->first();

        return $details->toArray();
    }

    /*
    * Update Bay details with id
    */
    public function updateBayData($id)
    {
        $selectBayApp = Bay::select('App','StationId', 'BayLocationCode', 'status')
                       ->where('StationId', \Input::get('StationName'))
                       ->where('BayLocationCode', \Input::get('BayLocationDesc'))
                        ->where('App', \Input::get('App'))
                        ->first();
        if($selectBayApp==null) {
            DB::table('Bay')->where('BayId', $id)
            ->update(array('StationId' => \Input::get('StationName'),
                            'BayLocationCode' => \Input::get('BayLocationDesc'),
                           'App' => \Input::get('App'),
                            'status' => \Input::get('Bay_status')));
        }
        else if($selectBayApp['status'] != \Input::get('Bay_status')) {
             DB::table('Bay')->where('BayId', $id)
            ->update(array('status' => \Input::get('Bay_status')));
        }
        else {
             $error = 'Duplicate Entry';
            throw new \Exception($error);
        }

    }

    /*
    * Autocomplete search for bay
    */

    public function autocompleteSearch($searchterm){
        $term = $searchterm;
        $colValue = \Request::segment(3);
        $results = array();

        $queries = DB::table('Bay')
                    ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                    ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode');

        switch ($colValue) {
            case 'StationName':
                $queries->where('Station.StationName', 'LIKE', '%'.$term.'%');
                break;
            case 'BayLocationDesc':
                $queries->where('BayLocation.BayLocationDesc', 'LIKE', '%'.$term.'%');
                break;
            case 'App':
                $queries->where('Bay.App', 'LIKE', '%'.$term.'%');
                break;
            }

        $queries->groupBy(\Request::segment(3));
        $res = $queries->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();

        foreach ($res as $query)
        {
            if(\Request::segment(3) == 'StationName'){
                $results[] = [ 'id' => $query->BayId, 'value' => $query->StationName];
            }
            else if(\Request::segment(3) == 'BayLocationDesc') {
                $results[] = [ 'id' => $query->BayId, 'value' => $query->BayLocationDesc];
            }
            else if(\Request::segment(3) == 'App') {
                $results[] = [ 'id' => $query->BayId, 'value' => $query->App];
            }
        }
        return \Response::json($results);
    }


    /*
    * Delete Bay details with id
    */
    public function deleteBay($id)
    {
        $result = DB::table('Bay')
            ->join('Host', 'Bay.BayId', '=', 'Host.HostId')
            ->where('Bay.BayId', $id)
            ->update(array( 'Bay.status' => 'D',  'Host.status' => 'D'));
        if($result) {
            return true;
        }
        else {
            $result = DB::table('Bay')
            ->where('Bay.BayId', $id)
            ->update(array('Bay.status' => 'D'));
            if($result){
                return true;
            }

        }
    }
}
