<?php
namespace App\Repositories;

use App\Models\User;
use App\Models\Bay;
use App\Models\Host;

use DB;

class HostRepository
{

    public  $validationRules = [
            //'HostName' => 'required|state_code:StateCode|station_name|counter_type|bay_location_code|hyphen|add_number|unique:Host',
            'HostName' => 'required|unique:Host',
            'CounterNumber' => 'required|numeric',
            'HostMachineId' => 'numeric',
            'HostMasterId'  => 'numeric',
            'BayId'  => 'required_if:BayLocationBayId,""',
            'StartTime' => 'required',
            'EndTIme' => 'required'
        ];

    // Update validation rule
    public function editValidationRules($host) {
        //dd($host);
             $rules = array(//'HostName' => 'required|state_code:StateCode|station_name|counter_type|bay_location_code|hyphen|add_number|unique:Host,HostName,'.$host['HostId'].',HostId',
                            'HostName' => 'required | unique:Host,HostName,'.$host['HostId'].',HostId',
                            'CounterNumber' => 'required|numeric',
                            'HostMachineId' => 'numeric',
                            'HostMasterId'  => 'numeric',
                            'BayId'  => 'required_if:BayLocationBayId,""',
                            'StartTime' => 'required',
                            'EndTIme' => 'required');
             return $rules;
        }

    /*
    * Insert host details to DB
    */
    public function insertData($inputData)
    {
        //dd($inputData);
        if (isset($inputData['BayLocationBayId']) && $inputData['BayLocationBayId'] != null){
            $inputData['BayId'] = $inputData['BayLocationBayId'];
        }
        if (isset($inputData['MasterOrPlayer'])) {
            $checkMaster = Host::where('BayId', $inputData['BayId'])->first();
            if($checkMaster == null && $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType')) {
                $masterPlayerBoth = \Config::get('vyoma-constants.masterType');
                $masterId = $inputData['HostId'];
                $masterName = $inputData['HostMasterName'];
                $sTime = $inputData['StartTime'];
                $eTime = $inputData['EndTIme'];
            }
            else if ($checkMaster == null && $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')) {
                $masterPlayerBoth = \Config::get('vyoma-constants.bothType');
                $masterId = $inputData['HostId'];
                $masterName = $inputData['HostMasterName'];
                $sTime = $inputData['StartTime'];
                $eTime = $inputData['EndTIme'];
            }
            else if ($checkMaster == null && $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.playerType')) {
                $error = 'Cannot create Players until create Master';
                throw new \Exception($error);
            }
            else {
                if($checkMaster->MasterOrPlayer == \Config::get('vyoma-constants.bothType') && $inputData['MasterOrPlayer'] == $checkMaster->MasterOrPlayer) {
                    $error = 'Host Type is Both, already Present!';
                    throw new \Exception($error);
                }
                else if($checkMaster->MasterOrPlayer == \Config::get('vyoma-constants.masterType') && $inputData['MasterOrPlayer'] == $checkMaster->MasterOrPlayer){
                    $error = 'Host Type is Master, already Present!';
                    throw new \Exception($error);
                }
                else {

                     if (strtotime($inputData['StartTime']) >= strtotime($checkMaster->StartTime)  &&  strtotime($inputData['EndTIme']) <= strtotime($checkMaster->EndTIme)) {
                        $sTime = $inputData['StartTime'];
                        $eTime = $inputData['EndTIme'];
                    }
                    else {
                        $strtTim = date('H:i', strtotime($checkMaster->StartTime));
                        $endtime = date('H:i', strtotime($checkMaster->EndTIme));
                        $error = "Please select time between Master's StartTime ".$strtTim." & EndTime ".$endtime;
                        throw new \Exception($error);
                    }
                    $masterPlayerBoth = \Config::get('vyoma-constants.playerType');
                    $masterId = $checkMaster->HostMasterId;
                    $masterName = $checkMaster->HostMasterName;
                }
            }
        }
        $endTIme = $this->timeValidation( $eTime, $sTime);
        $dataToInsert = array(
            'HostName' => $inputData['HostName'],
            'BayId' => $inputData['BayId'],
            'MasterOrPlayer' => $masterPlayerBoth,
            'HostCounterType' => $inputData['HostCounterType'],
            'HostMasterId' => $masterId,
            'HostMachineId' => "",
            'CounterNumber' => $inputData['CounterNumber'],
            'HostMasterName' => $masterName,
            'StartTime' =>$sTime,
            'EndTIme' => $endTIme,
            'status' => $inputData['Host_status']
        );
        $insertedId = DB::table('Host')->insertGetId($dataToInsert);
        if($insertedId){
            if($checkMaster == null && $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType') || $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')) {
                $result = DB::table('Host')->where('HostId', $insertedId)
                ->update(array('HostMasterId' => $insertedId));

                return $result;
            }
        }
    }

    public function timeValidation($endTime, $startTime) {
        if(strtotime($endTime) > strtotime($startTime) && strtotime($endTime) <= strtotime('23:59'))
             {
                 return $endTime;
             }
             else {
                $error = 'Please give valid StartTime and EndTime';
                throw new \Exception($error);
            }
    }

    public function getBayApp($bayId){
        $app = Bay::select('App')->where('BayId', $bayId)->first();

        return $app;
    }

    /*
    * Fetch bay list info with bay location code
    */
    public function baylist()
    {
        $status = \Config::get('vyoma-constants.status');
        $list = DB::table('Bay')->distinct()->where('Bay.status', $status['Active'])
                ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                ->lists('BayLocation.BayLocationDesc', 'Bay.BayId');
        return $list;
    }

    public function bayDetail($bayid)
    {
        $status = \Config::get('vyoma-constants.status');
        $list = Bay::where('Bay.status', $status['Active'])
                ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                ->where('BayId', $bayid)->lists('BayLocation.BayLocationDesc', 'Bay.BayId');
        return $list;
    }

    public function bayDetailTest($bayid)
    {
        $status = \Config::get('vyoma-constants.status');
        $list = Bay::where('Bay.status', $status['Active'])
                ->select('BayLocation.BayLocationDesc', 'Bay.BayId')
                ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                ->where('BayId', $bayid)->get();
        return $list;
    }

    public function stationDetail($bayid)
    {
        $station = Bay::select('Station.StationName','Station.StationId')->where('Bay.status', 'A')->where('Bay.BayId', $bayid)
                ->join('Station', 'Bay.StationId' ,'=', 'Station.StationId')->first();


        return $station->toArray();
    }

    /*
    * Master or player information
    */
    public function getPlayerOrMasterInfo($bayid)
    {
        $checkMaster = Host::where('BayId', $bayid)->get();
        return $checkMaster->toArray();

    }

    public function getHostId($host) {
        $host = Host::select('BayId')->where('HostName', $host)->first();
        return $host;
    }

    public function getHostIdwithName($host) {
        $host = Host::select('HostId', 'BayId', 'StartTime', 'EndTIme')->where('HostName', $host)->first();
        return $host;
    }

    public function getLocationInfo($bay, $host)
    {
        $bayLocation = DB::table('Host')
                            ->distinct()
                            ->select('BayLocation.BayLocationDesc')
                            ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
                            ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                            ->where('Host.BayId', $bay)
                            ->where('Host.HostId', $host)
                            ->first();

        return $bayLocation;
    }

    public function getBayinfoWithStations($station)
    {
        $status ='A';
           $detailsOfBay = DB::Select(DB::raw('SELECT CONCAT(BayLocation.BayLocationDesc, Bay.app) as BayLocationDesc, Bay.BayId FROM `BayLocation`
           JOIN `Bay` ON Bay.BayLocationCode = BayLocation.BayLocationCode
           WHERE Bay.StationId=:station AND Bay.status =:status
           '),array('station' => $station , 'status' =>$status));

            // $detailsOfBay = DB::SELECT()('BayLocation')
            // ->select('Bay.BayId', 'CONCAT(BayLocation.BayLocationDesc, Bay.app) AS BayLocationDesc')
            // ->join('Bay', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
            //                 ->where('Bay.StationId', $station)
            //                 ->where('Bay.status', 'A')
            //                 ->groupBy('BayLocation.BayLocationCode')->get();
            $bays = $detailsOfBay;


        return $bays;
    }

    /*
    * Update host info with id
    */
    public function updateHostDetails($hostid)
    {
        $status = \Config::get('vyoma-constants.status');
        $masterName = \Input::get('MasterOrPlayer');

        $sTime = \Input::get('StartTime');
        $eTime = \Input::get('EndTIme');

        $changeOfPlayers = Host::select('MasterOrPlayer', 'HostMasterId', 'BayId')
                                ->where('HostId', $hostid)->first();
        if($changeOfPlayers['BayId'] != \Input::get('BayId')) {
            $getBay = Host::select('MasterOrPlayer', 'HostMasterId', 'HostMasterName')
                            ->where('BayId', $changeOfPlayers['BayId'])->first();
            if(\Input::get('MasterOrPlayer') == \Config::get('vyoma-constants.playerType')) {


                $result = DB::table('Host')->where('HostId', $hostid)
                            ->update(array('HostMasterId' => $getBay['HostMasterId'],
                                           'HostMasterName' =>$getBay['HostMasterName']));

            }
        }
        if(\Input::get('MasterOrPlayer') == \Config::get('vyoma-constants.masterType')) {
            $masterId = Host::select('HostMasterId', 'MasterOrPlayer')
                                ->where('HostId', $hostid)->first();

            if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')) {
                $masterName = \Config::get('vyoma-constants.masterType');
            }
            else if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.playerType')){
               $count = Host::select('HostMasterId', 'MasterOrPlayer')
                            ->where('HostId', $hostid)->first();
                $hostmasterId = Host::where('HostId', $count['HostMasterId'])->get();
                if($hostmasterId != null){
                    $error = "Master already Present";
                    throw new \Exception($error);
                }

            }
            else {
                $count = Host::select('HostMasterId', 'MasterOrPlayer')
                                ->where('HostId', $hostid)->first();
                $hostmasterId = Host::where('HostId', $count['HostMasterId'])->get();
               /* if($hostmasterId != null){
                    $error = "Master already Present";
                    throw new \Exception($error);

                }*/
            }

            if(\Input::get('Host_status') == $status['Inactive']) {
                $inactiveHost = DB::table('Host')->where('HostMasterId', $masterId['HostMasterId'])
                                ->update(array('status' => $status['Inactive']));
            }
            else {
                $inactiveHost = DB::table('Host')->where('HostMasterId', $masterId['HostMasterId'])
                                ->update(array('status' => $status['Active']));
            }
        }
        else if(\Input::get('MasterOrPlayer') == \Config::get('vyoma-constants.playerType')){

            $masterId = Host::select('HostMasterId', 'MasterOrPlayer')
                                ->where('HostId', $hostid)->first();
            if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.playerType')) {
            $hostmasterTime = Host::select('StartTime', 'EndTIme')
                                ->where('HostId', $masterId['HostMasterId'])->first();

            if (strtotime($sTime) >= strtotime($hostmasterTime['StartTime'])  &&  strtotime($eTime) <= strtotime($hostmasterTime['EndTIme'])) {
                $sTime = \Input::get('StartTime');
                $eTime = \Input::get('EndTIme');
            }
            else {
                $strtTime = date('H:i', strtotime($hostmasterTime['StartTime']));
                $endTime = date('H:i', strtotime($hostmasterTime['EndTIme']));
                $error = "Please select time between Master's StartTime ". $strtTime. " & EndTime ".$endTime;
                throw new \Exception($error);
            }
        }
        else if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')) {
                //$masterName = \Config::get('vyoma-constants.bothType');
                $error = 'Cannot change Both type to Player';
                throw new \Exception($error);
            }
            else if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType')) {
                //$masterName = \Config::get('vyoma-constants.masterType');
                $error = 'Cannot change Master type to Player';
                throw new \Exception($error);
            }


        }
        else {
             $masterId = Host::select('HostMasterId', 'MasterOrPlayer')
                                ->where('HostId', $hostid)->first();
            if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType')) {
                $count = Host::select('HostMasterId', 'MasterOrPlayer')
                        ->where('HostMasterId', $masterId['HostMasterId'])->count();

                 $masterName = \Config::get('vyoma-constants.bothType');
                /*if($count > 1) {
                    $error = "Cannot Change Master to Both, it has Players";
                    throw new \Exception($error);
                }
                else if($count == 0 || $count == 1){
                    $masterName = \Config::get('vyoma-constants.bothType');
                }*/
            }
            else if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.playerType')) {
                    $error = "Cannot Change Player to Both, it has Master";
                    throw new \Exception($error);
            }
        }
        $endTIme = $this->timeValidation($eTime, $sTime);

        $result = DB::table('Host')->where('HostId', $hostid)
            ->update(array('HostName' => \Input::get('HostName'),
                            'BayId' => \Input::get('BayId'),
                            'MasterOrPlayer' => $masterName,
                            'HostCounterType' => \Input::get('HostCounterType'),
                            //'HostMasterId' => \Input::get('HostMasterId'),
                            'HostMachineId' => \Input::get('HostMachineId'),
                            'CounterNumber' => \Input::get('CounterNumber'),
                            'HostMasterName' => \Input::get('HostMasterName'),
                            'StartTime' => $sTime,
                            'EndTIme' => $endTIme,
                            'status' => \Input::get('Host_status')
                            ));
    }

    /*
    * Autocomplete search for Host
    */

     public function autocompleteSearch($searchterm){
        $term = $searchterm;
        $colValue = \Request::segment(3);
        $results = array();

        $queries = DB::table('Host')
                    ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
                    ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                    ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                    ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
                    ->join('Region', 'Station.RegionId', '=', 'Region.RegionId');

        switch ($colValue) {
            case 'StationName':
                $queries->where('Station.StationName', 'LIKE', '%'.$term.'%');
                break;
            case 'BayLocationDesc':
                $queries->where('BayLocation.BayLocationDesc', 'LIKE', '%'.$term.'%');
                break;
            case 'HostName':
                $queries->where('Host.HostName', 'LIKE', '%'.$term.'%');
                break;
            case 'HostMasterName':
                $queries->where('Host.HostMasterName', 'LIKE', '%'.$term.'%');
                break;
            case 'HostCounterType':
                $queries->where('Host.HostCounterType', 'LIKE', '%'.$term.'%');
                break;
            case 'CounterNumber':
                $queries->where('Host.CounterNumber', 'LIKE', '%'.$term.'%');
                break;
            case 'BranchName':
                $queries->where('Branch.BranchName', 'LIKE', '%'.$term.'%');
                break;
            case 'RegionName':
                $queries->where('Region.RegionName', 'LIKE', '%'.$term.'%');
                break;

        }

        $queries->groupBy(\Request::segment(3));
        $res = $queries->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();

        foreach ($res as $query)
        {
            if(\Request::segment(3) == 'StationName'){
                $results[] = [ 'id' => $query->HostId, 'value' => $query->StationName];
            }
            else if(\Request::segment(3) == 'BranchName') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->BranchName];
            }
            else if(\Request::segment(3) == 'RegionName'){
                $results[] = [ 'id' => $query->HostId, 'value' => $query->RegionName];
            }
            else if(\Request::segment(3) == 'BayLocationDesc') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->BayLocationDesc];
            }
            else if(\Request::segment(3) == 'HostName') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->HostName];
            }
            else if(\Request::segment(3) == 'HostMasterName') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->HostMasterName];
            }
            else if(\Request::segment(3) == 'HostCounterType') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->HostCounterType];
            }
            else if(\Request::segment(3) == 'CounterNumber') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->CounterNumber];
            }
        }
        return \Response::json($results);
    }

    /*
    * Delete host info with id
    */
    public function deleteHost($id)
    {
        $status = \Config::get('vyoma-constants.status');
        Host::where('HostId', $id)
            ->update(array('status' => $status['Delete']));
    }

    public function stationId($bayid)
    {
        $station = Bay::select('Station.StationId')->where('Bay.status', 'A')->where('Bay.BayId', $bayid)
                ->join('Station', 'Bay.StationId' ,'=', 'Station.StationId')->first();


        return $station;
    }
}
