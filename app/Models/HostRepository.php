<?php
namespace App\Models;

use Illuminate\Database\DatabaseManager as DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use DB;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

class HostRepository extends EloquentRepositoryAbstract
{
    public function __construct(Model $Model)
    {
        /*if(\Input::get('_search') == true){
            $filters = \Input::get('filters');
            dd($filters);
        }*/
        $this->status = \Config::get('vyoma-constants.status');
        if (request()->has('station_id')) {

            $this->Database = DB::table('Host')
              ->select('Host.HostId', 'Host.HostName' , 'Station.StationName', 'BayLocation.BayLocationDesc', 'Host.MasterOrPlayer','Host.HostCounterType','Host.CounterNumber','Host.HostMasterName','Branch.BranchName',
                            'Region.RegionName','Host.StartTime','Host.EndTIme', DB::raw('if(Host.status = "A", "Active", "Inactive") as "Host.status"'))
              ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
              ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
              ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
              ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
              ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
              ->where('Bay.StationId', request()->get('station_id'))
              ->where('Host.status', $this->status["Active"]);
        } else if (request()->has('bay_id') && request()->has('app')){
            $app = request()->get('app');
            $bayid = request()->get('bay_id');
            $station =  DB::table('Bay')->select('Station.StationName','Station.StationId')
                        ->where('Bay.status', $this->status["Active"])
                        ->where('Bay.BayId', $bayid)
                        ->where('Bay.app', $app)
                        ->join('Station', 'Bay.StationId' ,'=', 'Station.StationId')->first();
            $stationId = $station->StationId;
            $this->Database = DB::table('Host')
            ->select('Host.HostId', 'Host.HostName' , 'Station.StationName', 'BayLocation.BayLocationDesc', 'Host.MasterOrPlayer','Host.HostCounterType','Host.CounterNumber','Host.HostMasterName','Branch.BranchName',
                            'Region.RegionName','Host.StartTime','Host.EndTIme', DB::raw('if(Host.status = "A", "Active", "Inactive") as "Host.status"'))
              ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
              ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
              ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
              ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
              ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
              ->where('Bay.StationId', $stationId)
              ->where('Bay.app', $app)
              ->where('Host.status', $this->status["Active"]);
        }
        else if(request()->has('region_id')){
            $RegionId = request()->get('region_id');
            $this->Database  =  DB::table('Host')
                            ->select('Host.HostId', 'Host.HostName' , 'Station.StationName', 'BayLocation.BayLocationDesc', 'Host.MasterOrPlayer','Host.HostCounterType','Host.CounterNumber','Host.HostMasterName','Branch.BranchName',
                            'Region.RegionName','Host.StartTime','Host.EndTIme', DB::raw('if(Host.status = "A", "Active", "Inactive") as "Host.status"'))
                            ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
                            ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                            ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
                            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
                            ->where('Station.RegionId', $RegionId)
                            ->where('Station.status', 'A')
                            ->where('Region.status', 'A')
                            ->where('Bay.status', 'A')
                            ->where('Host.status', 'A');
        }
        else {
            $this->Database = DB::table('Host')
                          ->select('Host.HostId', 'Host.HostName' , 'Station.StationName', 'BayLocation.BayLocationDesc', 'Host.MasterOrPlayer','Host.HostCounterType','Host.CounterNumber','Host.HostMasterName','Branch.BranchName',
                            'Region.RegionName','Host.StartTime','Host.EndTIme', DB::raw('if(Host.status = "A", "Active", "Inactive") as "Host.status"'))
                          ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
                          ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                          ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                          ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
                          ->join('Region', 'Station.RegionId', '=', 'Region.RegionId');

        }
        $this->visibleColumns = array('Host.HostId', 'Host.HostName' , 'Station.StationName', 'BayLocation.BayLocationDesc', 'Host.MasterOrPlayer','Host.HostCounterType','Host.CounterNumber','Host.HostMasterName','Branch.BranchName','Region.RegionName','Host.StartTime','Host.EndTIme','Host.status AS Host.status');
        $this->orderBy = array(array('Station.StationName'), array('BayLocation.BayLocationDesc'), array('Host.HostName'));
    }
}
