<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Station';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['StationName','RegionId','BranchId','StateId', 'status'];

    public function Branch()
    {
        return $this->belongsTo('Branch');
    }

    public function Region()
    {
        return $this->belongsTo('Region');
    }
}
