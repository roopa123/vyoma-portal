<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Baylocation;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

class BaylocationRepository extends EloquentRepositoryAbstract
{
    public function __construct(Model $Model)
    {
        $this->Database = Baylocation::select('BayLocationCode', 'BayLocationDesc', DB::raw('if(status = "A", "Active", "Inactive") as "status"'));

        $this->visibleColumns = array('BayLocationCode', 'BayLocationDesc', 'status');

        $this->orderBy = array(array('BayLocationCode'));
    }
}
