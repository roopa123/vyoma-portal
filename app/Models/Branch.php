<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    /**
     * The database table used by the Branch model.
     *
     * @var string
     */
    protected $table = 'Branch';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['BranchName', 'status'];

    public function Station()
    {
        return $this->hasMany('Station');
    }
}
