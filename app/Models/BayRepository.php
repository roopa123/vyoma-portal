<?php
namespace App\Models;

use Illuminate\Database\DatabaseManager as DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use DB;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

class BayRepository extends EloquentRepositoryAbstract
{

    public function __construct(Model $Model)
    {
        //dd(\Input::get('filters'));
        // if(\Input::get('_search') == 'true') {
        //     $filters = json_decode(\Input::get('filters'));
        //     $filters->rules[0]->field = 'Bay.status';
        //     return json_encode($filters);
        // }

        if (request()->has('region_id')) {
            $RegionId = request()->get('region_id');
            $this->Database =  Bay::select('Bay.BayId', 'Station.StationName' ,'BayLocation.BayLocationDesc', 'Bay.App',
                            DB::raw('if(Bay.status = "A", "Active", "Inactive") as "Bay.status"'))
                        ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                        ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                        ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
                        ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
                        ->where('Station.RegionId', $RegionId)
                        ->where('Station.status', 'A')
                        ->where('Region.status', 'A')
                        ->where('Bay.status', 'A');
        }
        else {
            $this->Database = DB::table('Bay')
                        ->select('Bay.BayId', 'Station.StationName' ,'BayLocation.BayLocationDesc', 'Bay.App',
                            DB::raw('if(Bay.status = "A", "Active", "Inactive") as "Bay.status"'))
                        ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                        ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                        ->where('Station.status','A');
        }
        $this->visibleColumns = array('Bay.BayId', 'Station.StationName' ,'BayLocation.BayLocationDesc', 'Bay.App', 'Bay.status AS Bay.status');
        $this->orderBy = array(array('Station.StationName'), array('BayLocation.BayLocationDesc'));
    }
}
