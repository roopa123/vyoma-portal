<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Region';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['RegionName', 'status'];

    public function Station()
    {
        return $this->hasMany('Station');
    }
}
