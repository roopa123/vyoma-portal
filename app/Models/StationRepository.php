<?php
namespace App\Models;

use Illuminate\Database\DatabaseManager as DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use DB;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

class StationRepository extends EloquentRepositoryAbstract
{
    public function __construct(Model $Model)
    {
        //dd(request()->has('region_id'));
        if (request()->has('branch_id')) {
            $this->Database = DB::table('Station')
                            ->select('Station.StationId', 'Station.StationName',
                            'Region.RegionName', 'Branch.BranchName', 'State.StateName', DB::raw('if(Station.status = "A", "Active", "Inactive") as "Station.status"'))
                            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
                            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
                            ->join('State', 'Station.StateId', '=', 'State.StateId')
                            ->where('Station.BranchId', request()->get('branch_id'))
                            ->where('Station.status', 'A');
        } else if (request()->has('region_id')) {
            $this->Database = DB::table('Station')
            ->select('Station.StationId', 'Station.StationName',
            'Region.RegionName', 'Branch.BranchName', 'State.StateName', DB::raw('if(Station.status = "A", "Active", "Inactive") as "Station.status"'))
            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
            ->join('State', 'Station.StateId', '=', 'State.StateId')
            ->where('Station.RegionId', request()->get('region_id'))
            ->where('Station.status', 'A');
           }
           else if (request()->has('home_region_id')) {
            $this->Database = DB::table('Station')
            ->select('Station.StationId', 'Station.StationName',
            'Region.RegionName', 'Branch.BranchName', 'State.StateName', DB::raw('if(Station.status = "A", "Active", "Inactive") as "Station.status"'))
            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
            ->join('State', 'Station.StateId', '=', 'State.StateId')
            ->where('Station.RegionId', request()->get('home_region_id'))
            ->where('Station.status', 'A')
            ->where('Region.status', 'A');
           } else {
            $this->Database = DB::table('Station')
            ->select('Station.StationId', 'Station.StationName',
            'Region.RegionName', 'Branch.BranchName', 'State.StateName', DB::raw('if(Station.status = "A", "Active", "Inactive") as "Station.status"'))
            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
            ->join('State', 'Station.StateId', '=', 'State.StateId');
            //->where('Station.status', 'A');
        }
        $this->visibleColumns = array('Station.StationId', 'Station.StationName',
            'Region.RegionName', 'Branch.BranchName', 'State.StateName', 'Station.status AS Station.status');
        $this->orderBy = array(array('StationName'));
    }
}
