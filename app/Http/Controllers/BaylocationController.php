<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\BaylocationRepository;
use Illuminate\Http\Request;
use DB;

class BaylocationController extends Controller
{
    /**
    * Instances of Admin Repository
    */
    protected $baylocationRepo;

    /**
    * Access all methods and objects in Repository
    */

    public function __construct(BaylocationRepository $baylocationRepo)
    {
        $this->baylocationRepo = $baylocationRepo;
    }

    //Returning view
    public function getIndex()
    {
        return view('bayLocation.baylocation');
    }

    /*
     Saving information of BAyLocation
    */
    public function saveBayLocation()
    {
        $validator = \Validator::make(request()->all(), $this->baylocationRepo->validationRules,
                        ['BayLocationCode.regex' => \Lang::get('validation.BayLocationCode'),
                        'BayLocationDesc.required' => \Lang::get('vyoma-messages.BayLocationDesc'),
                        'BayLocationDesc.regex' => \Lang::get('vyoma-messages.BayLocationDescAlpha')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422), 422);
        }

        try {
            $this->baylocationRepo->insertData(\Input::all());
        } catch (QueryException $e) {
            return json_encode(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }

        return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.bayLocation').\Lang::get('vyoma-messages.createSuccess')));
    }

    /*
     Update the BAy Location Info
    */
    public function updateBayLOcation($code)
    {
        $validator = \Validator::make(request()->all(), $this->baylocationRepo->editValidationRules(\Input::all()),
                        ['BayLocationCode.regex' => \Lang::get('validation.BayLocationCode'),
                        'BayLocationDesc.required' => \Lang::get('vyoma-messages.BayLocationDesc'),
                        'BayLocationDesc.regex' => \Lang::get('vyoma-messages.BayLocationDescAlpha')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422), 422);
        }
        try {
            $this->baylocationRepo->updateBayLocation($code, \Input::all());
        } catch (Exception $e) {
            return json_encode(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }

        return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.bayLocation').\Lang::get('vyoma-messages.updateSuccess')));
    }

    /*
     Delete The Bay Location Information
    */
    public function deleteBayLOcation($code)
    {
        try {
            $this->baylocationRepo->deleteBayLocation($code);
        } catch (Exception $e) {
            return json_encode(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }

        return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.bayLocation').\Lang::get('vyoma-messages.deleteSuccess')));
    }
    /*
    * AutoComplete for Region
    */

    public function autocomplete(){
        $searchValues = $this->baylocationRepo->autocompleteSearch(\Input::get('term'));

        return $searchValues;
    }

}
