<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\BayRepository;
use App\Repositories\StationRepository;
use App\Repositories\BaylocationRepository;
use Illuminate\Http\Request;
use DB;

class BayController extends Controller
{
    /**
     * Instances of Admin Repository
    */
    protected $stationRepo;
    protected $bayRepo;
    protected $baylocationRepo;
    /**
    * Access all methods and objects in Repository
    */
    public function __construct(
        StationRepository $stationRepo,
        BayRepository $bayRepo,
        BaylocationRepository $baylocationRepo
    ) {
           $this->stationRepo = $stationRepo;
           $this->bayRepo = $bayRepo;
           $this->baylocationRepo = $baylocationRepo;
    }

    /*
     Fetching details of BayLocation List and return it to view
    */
    public function getIndex()
    {
        $buildUrl = \URL::to('/bay-grid');
        $status = '';
        $searchOption = array('' => 'All', 'A' => 'Active', 'I' => 'Inactive');
        if (\Request::segment(2) == 'region') {
            $status = \Request::segment(5);
            $id['region_id'] = \Request::segment(3);
            $searchOption = array('A' => 'Active');
            $buildUrl = \URL::to('/bay-grid'. '?region_id='.$id['region_id']);

        }
        $listOfStations = $this->stationRepo->stationlist();
        $listOfBayLocations = $this->baylocationRepo->baylocationlist();
        return view('bay.bay', compact('listOfStations', 'listOfBayLocations', 'buildUrl', 'status', 'searchOption'));
    }

    /*
     Saving details of BayLocation
    */
    public function saveBayInfo()
    {
        $validator = \Validator::make(request()->all(), $this->bayRepo->validationRules, ['BayLocationDesc.required' => \Lang::get('vyoma-messages.BayLocationDesc')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422), 422);
        }
        try {
            $this->bayRepo->insertData(\Input::all());
        } catch (\Exception $e)
         {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning'), 'errors'=>['duplicate'=>[$e->getMessage()]] , 'status' => 422), 422);
        }

        return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.bay').\Lang::get('vyoma-messages.createSuccess')));
    }

    /*
     Update the Bay Information
    */
    public function updateBayInfo($bayid)
    {
        try {
            $this->bayRepo->updateBayData($bayid, \Input::all());
        } catch (\Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning'), 'errors'=>['duplicate'=>[$e->getMessage()]] , 'status' => 422), 422);
        }

        return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.bay').\Lang::get('vyoma-messages.updateSuccess')));
    }

    /*
     Delete the Bay Information
    */
    public function deleteBayInfo($bayid)
    {
        try {
            $this->bayRepo->deleteBay($bayid);
        } catch (Exception $e) {
            return json_encode(array('success' => false, 'message' => 'Something went wrong, please try again later.'));
        }

        return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.bay').\Lang::get('vyoma-messages.deleteSuccess')));
    }

    /*
     Get station name based on bay location code
    */
    public function getStationInfo($code)
    {
       return $info = $this->bayRepo->getStationDetail($code);

    }

    /*
    * AutoComplete for bay search
    */
    public function autocomplete(){
        $searchValues = $this->bayRepo->autocompleteSearch(\Input::get('term'));

        return $searchValues;
    }

}
