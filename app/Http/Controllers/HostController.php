<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\BayRepository;
use App\Repositories\HostRepository;
use Illuminate\Http\Request;
use App\Repositories\StationRepository;
use App\Http\Requests\StoreHostNameRequest;
use DB;

class HostController extends Controller
{
    /**
    * Instances of Admin Repository
    */
    protected $hostRepo;
    protected $bayRepo;
    /**
    * Access all methods and objects in Repository
    */

    public function __construct(
        BayRepository $bayRepo,
        HostRepository $hostRepo,
        StationRepository $stationRepo
    ) {
        $this->bayRepo = $bayRepo;
        $this->hostRepo = $hostRepo;
        $this->stationRepo = $stationRepo;
    }

    /*
     Fetch info of station and bay, returning it to view
    */
    public function getIndex()
    {
        $status = '';
        $searchOption = array('' => 'All', 'A' => 'Active', 'I' => 'Inactive');
        if (\Request::segment(2) == 'region') {
            $id['region_id'] = \Request::segment(3);
            $buildUrl = \URL::to('/host-grid'. '?region_id='.$id['region_id']);
            $listOfBays = $this->hostRepo->baylist();
            $bayID = "";
            $DetailsOfStation = $this->stationRepo->stationlist();
            $editTypeBayLocation = "select";
            $editTypeStation = "select";
            $editable = false;
            $stationName = "";
            $status = \Request::segment(5);
            $searchOption = array('A' => 'Active');
        }
        else if (\Request::segment(2) == 'station') {
            $id['station_id'] = \Request::segment(3);
            $buildUrl = \URL::to('/host-grid'. '?station_id='.$id['station_id']);
            $listOfBays = $this->hostRepo->baylist();
            $bayID = "";
            $DetailsOfStation = $this->stationRepo->stationlist();
            $editTypeBayLocation = "select";
            $editTypeStation = "select";
            $editable = false;
            $stationName = "";
        } else if (\Request::segment(2) == 'bay') {
            $idOfBay = \Request::segment(3);
            $id['bay_id'] = $idOfBay;
            $bayApp = $this->hostRepo->getBayApp($id['bay_id']);
            //dd($bayApp['App']);
            $buildUrl = \URL::to('/host-grid'. '?bay_id='.$id['bay_id']. '&app='.$bayApp['App']);
            $bays = $this->hostRepo->bayDetailTest($id['bay_id']);
            $listOfBays = $bays[0]['BayLocationDesc'];
            $bayID =$bays[0]['BayId'];
            $station = $this->hostRepo->stationDetail($id['bay_id']);
            $DetailsOfStation = $station['StationName'];
            $stationName = "For ".$DetailsOfStation;
            $editTypeBayLocation = "text";
            $editTypeStation = "text";
            $editable = true;
        } else {
            $id['id'] = "";
            $bayID = "";
            $buildUrl = \URL::to('/host-grid');
            $listOfBays = $this->hostRepo->baylist();
            $DetailsOfStation = $this->stationRepo->stationlist();
            $editTypeBayLocation = "select";
            $editTypeStation = "select";
            $editable = false;
            $stationName = "";
        }
        $redirect = '';
        if(\Request::segment(4) == 'redirect'){
            $redirect = '/redirect/true';
        }
        return view('host.host', compact('listOfBays', 'buildUrl','DetailsOfStation','redirect',
         'editTypeBayLocation', 'editTypeStation', 'bayID', 'editable', 'stationName', 'status', 'searchOption'));
    }

    /*
     Save Host Information
    */
    public function saveHostInfo()
    {
        $validator = \Validator::make(request()->all(), $this->hostRepo->validationRules);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => 'Follow the rules for Host Name','errors'=>$validator->errors(), 'status' => 422), 422);
        }
        try {
            $this->hostRepo->insertData(\Input::all());
        } catch (\Exception $e) {
            //dd($e->getMessage());
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning'), 'errors'=>['invalid'=>[$e->getMessage()]] , 'status' => 422), 422);

        }

        if (\Request::segment(3) == 'redirect') {
            return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.host').\Lang::get('vyoma-messages.createSuccess'), 'redirect' => true));
        }
        else {

            return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.host').\Lang::get('vyoma-messages.createSuccess'), 'redirect' => false));
        }
    }

    /*
    * Get player or master info based on bay id
    */
    public function getPlayerInfo($bayid)
    {
        $masterName = "";
        $checkMaster = $this->hostRepo->getPlayerOrMasterInfo($bayid);
        foreach ($checkMaster as $value) {
            if($value['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType')) {
                $masterName = $value['HostMasterName'];
            }
            else if($value['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')){
                $masterName = $value['HostMasterName'];
            }
        }

        return $masterName;
    }

    public function getBayInfo($station_id){
        if($details = $this->hostRepo->getBayinfoWithStations($station_id)){
             return $details;
        }
    }

    public function getHostname($host) {
        $info = $this->hostRepo->getHostId($host);
        //dd($info['BayId']);
        return $info;
    }

    public function getHostId($host) {
        $info = $this->hostRepo->getHostIdwithName($host);
        //dd($info['BayId']);
        return $info;
    }

    public function getBayLocationInfo($bay, $host) {
        $info = $this->hostRepo->getLocationInfo($bay, $host);
        //dd(response()->json($info));
        return response()->json($info);
    }

    public function updateHostInfo($hostid)
    {
        $validator = \Validator::make(request()->all(), $this->hostRepo->editValidationRules(\Input::all()));

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => 'Follow the rules for Host Name','errors'=>$validator->errors(), 'status' => 422), 422);
        }
      try {
            $this->hostRepo->updateHostDetails($hostid, \Input::all());
        } catch (\Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning'), 'errors'=>['invalid'=>[$e->getMessage()]] , 'status' => 422), 422);
        }
        return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.host').\Lang::get('vyoma-messages.updateSuccess')));
    }

    public function deleteHostInfo($hostid)
    {
        try {
            $this->hostRepo->deleteHost($hostid);
        } catch (Exception $e) {
            return json_encode(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }

        return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.host').\Lang::get('vyoma-messages.deleteSuccess')));
    }

    /*
    * AutoComplete for Station search
    */

    public function autocomplete(){
        $searchValues = $this->hostRepo->autocompleteSearch(\Input::get('term'));

        return $searchValues;
    }

}
