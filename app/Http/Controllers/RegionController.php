<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\RegionRepository;
use Illuminate\Http\Request;
use DB;

class RegionController extends Controller
{
    /**
    * Instances of Admin Repository
    */
    protected $regionRepo;

    /**
    * Access all methods and objects in Repository
    */
    public function __construct(
        RegionRepository $regionRepo
    ) {
        $this->regionRepo = $regionRepo;
    }

    /*
     Retruning view
    */
    public function getIndex()
    {
        /*if (\Request::segment(2) == 'region') {
            $id['region_id'] = \Request::segment(3);
            $buildUrl = \URL::to('/region-grid'. '?region_id='.$id['station_id']);
        }
        else {

        }*/
        return view('region.region');
    }

    /*
     Save Region information
    */
    public function saveRegion()
    {
        $validator = \Validator::make(request()->all(), $this->regionRepo->validationRules,  ['RegionName.regex' => \Lang::get('validation.hyphenRegex')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422), 422);
        }
        try {
            $this->regionRepo->insertData(\Input::all());
        } catch (Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }

        return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.region').\Lang::get('vyoma-messages.createSuccess')));
    }

    /*
     Update Region information with Region id
    */
    public function updateRegion($regid)
    {
        $validator = \Validator::make(request()->all(), $this->regionRepo->editValidationRules(\Input::all()), ['RegionName.regex' => \Lang::get('validation.hyphenRegex')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422), 422);
        }
        try {
            $this->regionRepo->updateRegionDetails($regid, \Input::all());
        } catch (Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }

        return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.region').\Lang::get('vyoma-messages.updateSuccess')));
    }

    /*
     Delete Region information with Region id
    */
    public function deleteRegion($regid)
    {
        try {
            $this->regionRepo->deleteRegionrow($regid);
        } catch (Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }

        return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.region').\Lang::get('vyoma-messages.deleteSuccess')));
    }

    /*
    * AutoComplete for Region
    */
    public function autocomplete(){
        $searchValues = $this->regionRepo->autocompleteSearch(\Input::get('term'));

        return $searchValues;
    }
}
