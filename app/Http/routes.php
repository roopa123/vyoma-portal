<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
|
*/

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['middleware' => 'auth'], function () {

/**
 *-------------------------------
 * Branch Crud
 *-------------------------------
 */
    // Default Landing Page
    Route::get('/', ['as' => 'Branch Home', 'uses' => 'HomeController@getIndex']);
    Route::get('/branch', ['as' => 'Branch Home', 'uses' => 'BranchController@getIndex']);
    Route::get('/home', ['as' => 'Home', 'uses' => 'HomeController@getIndex']);
    Route::post('/dashboard', ['as' => 'Branch Home', 'uses' => 'HomeController@getStatisticData']);

    // Get Data based on the Search cretiera
    Route::post('/branch-grid', ['as' => 'Branch Data', 'uses' =>  function()
    {
        GridEncoder::encodeRequestedData(new \App\Models\BranchRepository(new \App\Models\Branch()), Input::all());
    }]);
    // branch Add and Edit routes
    Route::post('/branch/crud', ['as' => 'Branch CRUD', 'uses' => function()
    {
      $branchController = new \App\Http\Controllers\BranchController(new \App\Repositories\BranchRepositories);

      switch (Input::get('oper'))
      {
        case 'add':
          return $branchController->storeBranch(Input::except('id', 'oper'));
          break;
        case 'edit':
          return $branchController->updateBranch(Input::get('BranchId'), Input::except('id', 'oper'));
          break;
        case 'del':
          return  $branchController->deleteBranch(Input::get('id'));
          break;
      }
    }]);

  /**
   *-------------------------------
   * Region Crud
   *-------------------------------
  */
  Route::get('region', ['as' => 'Region Home', 'uses' => 'RegionController@getIndex']);
  Route::post('/region-grid', ['as' => 'Region Data', 'uses' => function()
  {
      GridEncoder::encodeRequestedData(new \App\Models\RegionRepository(new \App\Models\Region()), Input::all());
  }]);
  Route::post('/region/crud', ['as' => 'Region CRUD', 'uses' => function()  {
    $regionController = new \App\Http\Controllers\RegionController(new \App\Repositories\RegionRepository);

    switch (Input::get('oper'))
    {
      case 'add':
        return $regionController->saveRegion(Input::except('id', 'oper'));
        break;
      case 'edit':
        return $regionController->updateRegion(Input::get('RegionId'), Input::except('id', 'oper'));
        break;
      case 'del':
        return  $regionController->deleteRegion(Input::get('id'));
        break;
    }
  }]);

  /**
   *-------------------------------
   * Station Crud
   *-------------------------------
  */
  Route::get('station', ['as' => 'Station Home', 'uses' => 'StationController@getIndex']);
  Route::get('station/{type}/{Id}', ['as' => 'Station Search', 'uses' => 'StationController@getIndex']);
  Route::post('/station-grid', ['as' => 'Station Data', 'uses' => function()
  {
      GridEncoder::encodeRequestedData(
        new \App\Models\StationRepository(new \App\Models\Station()), Input::all()
      );
  }]);
  Route::post('/station/crud', ['as' => 'Station CRUD', 'uses' => function()  {
    $stationController = new \App\Http\Controllers\StationController(new \App\Repositories\StationRepository, new \App\Repositories\RegionRepository);

    switch (Input::get('oper'))
    {
      case 'add':
        return $stationController->saveStation(Input::except('id', 'oper'));
        break;
      case 'edit':
        return $stationController->updateStationDetails(Input::get('StationId'), Input::except('id', 'oper'));
        break;
      case 'del':
        return  $stationController->deleteStation(Input::get('id'));
        break;
    }
  }]);

  /**
   *-------------------------------
   * Bay Location Crud
   *-------------------------------
  */
  Route::get('baylocation', ['as' => 'BayLocationHome', 'uses' => 'BaylocationController@getIndex']);
  Route::post('/baylocation-grid', ['as' => 'BayLocationData', 'uses' => function()   {
      GridEncoder::encodeRequestedData(new \App\Models\BaylocationRepository(new \App\Models\Baylocation()), Input::all());
  }]);
  Route::post('/baylocation/crud', ['as' => 'BayLocationCRUD', 'uses' => function() {
    $stationController = new \App\Http\Controllers\BaylocationController(new \App\Repositories\BaylocationRepository);

    switch (Input::get('oper'))
    {
      case 'add':
        return $stationController->saveBayLocation(Input::except('id', 'oper'));
        break;
      case 'edit':
        return $stationController->updateBayLocation(Input::get('BayLocationCode'), Input::except('id', 'oper'));
        break;
      case 'del':
        return  $stationController->deleteBayLocation(Input::get('id'));
        break;
    }
  }]);

  /**
   *-------------------------------
   * Bay  Crud
   *-------------------------------
  */
  Route::get('bay', ['as' => 'Bay Home', 'uses' => 'BayController@getIndex']);
  Route::get('bay/{type}/{Id}/status/{statusVal}', ['as' => 'Bay Search', 'uses' => 'BayController@getIndex']);
  Route::post('/bay-grid', ['as' => 'Bay Data', 'uses' => function() {
      GridEncoder::encodeRequestedData(new \App\Models\BayRepository(new \App\Models\Bay()), Input::all());
  }]);
  Route::post('/bay/crud', ['as' => 'Bay CRUD', 'uses' => function() {
    $bayController = new \App\Http\Controllers\BayController(new \App\Repositories\StationRepository, new \App\Repositories\BayRepository, new \App\Repositories\BaylocationRepository);

    switch (Input::get('oper'))
    {
      case 'add':
        return $bayController->saveBayInfo(Input::except('id', 'oper'));
        break;
      case 'edit':
        return $bayController->updateBayInfo(Input::get('BayId'), Input::except('id', 'oper'));
        break;
      case 'del':
        return  $bayController->deleteBayInfo(Input::get('id'));
        break;
    }
  }]);

  /**
   *-------------------------------
   * Host Crud
   *-------------------------------
  */
  Route::get('host', ['as' => 'Host Home', 'uses' => 'HostController@getIndex']);
  Route::get('host/{type}/{Id}/{redirect}', ['as' => 'Host Search', 'uses' => 'HostController@getIndex']);
  Route::get('host/{type}/{Id}/status/{statusVal}', ['as' => 'Host Search', 'uses' => 'HostController@getIndex']);
  Route::get('host/{type}/{Id}', ['as' => 'Host Search', 'uses' => 'HostController@getIndex']);

  Route::post('/host-grid', ['as' => 'Host Data', 'uses' => function() {
      GridEncoder::encodeRequestedData(new \App\Models\HostRepository(new \App\Models\Host()), Input::all());
  }]);
  Route::post('/host/crud/redirect/true', ['as' => 'Host CRUD', 'uses' => function() {
    $hostController = new \App\Http\Controllers\HostController(new \App\Repositories\BayRepository, new \App\Repositories\HostRepository, new \App\Repositories\StationRepository);

    switch (Input::get('oper'))
    {
      case 'add':
        return $hostController->saveHostInfo(Input::except('id', 'oper'));
        break;
    }
  }]);
   Route::post('/host/crud', ['as' => 'Host CRUD', 'uses' => function() {
    $hostController = new \App\Http\Controllers\HostController(new \App\Repositories\BayRepository, new \App\Repositories\HostRepository, new \App\Repositories\StationRepository);

    switch (Input::get('oper'))
    {
      case 'add':
        return $hostController->saveHostInfo(Input::except('id', 'oper'));
        break;
      case 'edit':
        return $hostController->updateHostInfo(Input::get('HostId'), Input::except('id', 'oper'));
        break;
      case 'del':
        return  $hostController->deleteHostInfo(Input::get('id'));
        break;
    }
  }]);

  Route::get('stationinfo/{bay_id}', ['as' => 'Station Info',
        'uses' => 'BayController@getStationInfo']);
  Route::get('playerinfo/{bay_id}', 'HostController@getPlayerInfo');
  Route::get('bayInfo/{station_id}', 'HostController@getBayInfo');
  Route::get('getCount', 'BranchController@getCount');
  Route::get('getbayId/{host}', 'HostController@getHostname');
  Route::get('getHostId/{host}', 'HostController@getHostId');
  Route::get('getbayLocation/{bayId}/{hostid}', 'HostController@getBayLocationInfo');

  Route::get('search/branchAutocomplete/{column}', 'BranchController@autocomplete');
  Route::get('search/regionAutocomplete/{column}', 'RegionController@autocomplete');
  Route::get('search/stationAutocomplete/{column}', 'StationController@autocomplete');
  Route::get('search/bayLocationAutocomplete/{column}', 'BaylocationController@autocomplete');
  Route::get('search/bayAutocomplete/{column}', 'BayController@autocomplete');
  Route::get('search/hostAutocomplete/{column}', 'HostController@autocomplete');
});








