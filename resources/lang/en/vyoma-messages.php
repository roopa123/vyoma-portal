<?php

return [
'warning' => 'Something went wrong, please try again later.',
'alreadyExists' => 'Already Exists!! Try with New name',
'branch' => 'Branch ',
'region' => 'Region ',
'bay' => 'Bay ',
'bayLocation' => 'Bay Location ',
'station' => 'Station ',
'host' => 'Host ',
'createSuccess' => 'created successfully',
'updateSuccess' => 'updated successfully',
'deleteSuccess' => 'deleted successfully',
'BayLocationDesc'      => 'The Bay Location Name is required',
'BayLocationDescAlpha'      => 'The Bay Location Name can contain only Alphabets, Spaces and Hyphen.',

];
