@extends('layouts.apptemplate')

@section('title')
    Hosts
@endsection

@section('menu')
@include('layouts.menu')
@endsection


@section('content')
@include('layouts.validationAlert')
<div class="">
    <div class="">
 <script type="text/javascript">

  function viewBaylocationInfo(formId){
    getHostInfo($("#v_HostName").text().replace(/\s+/g, ''));
    var start = $('#v_StartTime').text().slice(0, -3);
    var end =$('#v_EndTIme').text().slice(0, -3);
    $('#v_StartTime').text(start);
    $('#v_EndTIme').text(end);

    $('.ui-icon.ui-icon-triangle-1-e').click(function(){
      getHostInfo($("#v_HostName").text().replace(/\s+/g, ''));
      getHostTime($("#v_HostName").text().replace(/\s+/g, ''));
    });

    $('.ui-icon.ui-icon-triangle-1-w').click(function(){
      getHostInfo($("#v_HostName").text().replace(/\s+/g, ''));
      getHostTime($("#v_HostName").text().replace(/\s+/g, ''));
    });

    function getHostTime(host) {
      console.log(host);
      $.ajax({
              url: '/getHostId/'+host,
              type: "GET",
              success: function (data) {
                  var start = data.StartTime.slice(0, -3);
                  var end =data.EndTIme.slice(0, -3);
                  $('#v_StartTime').text(start);
                  $('#v_EndTIme').text(end);
              }
            });
    }

    function getHostInfo(host) {
      console.log(host);
      $.ajax({
              url: '/getHostId/'+host,
              type: "GET",
              success: function (data) {
                  getBayLocation(data.BayId, data.HostId);
              }
            });
    }

    function getBayLocation(bayId, hostId) {
      $.ajax({
              url: '/getbayLocation/'+bayId+'/'+hostId,
              type: "GET",
              success: function (data) {
                  $("#v_BayId").text(data.BayLocationDesc);
              }
            });
    }

  }

 function intializeBefore(formId) {
  console.log('m in init');
  console.log($("#HostName").val());
  getHostBayId($("#HostName").val());

  function getHostBayId(hostname) {
    $.ajax({
              url: '/getbayId/'+hostname,
              type: "GET",
              success: function (data) {
                console.log(data.BayId)
                  updateBayLocationinit($("#StationName").val(), data.BayId);
              }
            });
  }

    function updateBayLocationinit(stationId, setselected) {
            $.ajax({
              url: '/bayInfo/'+stationId,
              type: "GET",
              success: function (data) {
                var sn = data;
                console.log(setselected)

              $('#BayId').empty();
              console.log(setselected);

              $.each(data, function(i, obj){
                console.log(obj.BayId);
                  if(setselected === obj.BayId) {
                    $('#BayId').append($('<option>').text(obj.BayLocationDesc).attr('value', setselected).attr("selected",true));
                  }
                  else {
                    $('#BayId').append($('<option>').text(obj.BayLocationDesc).attr('value', obj.BayId));
                  }
            });
          }
        });
    }
 }


  function editBefore(formId) {
   var starttime = $('#StartTime').val();
   var endtime = $('#EndTIme').val();
   starttime = starttime.slice(0, -3);
   endtime = endtime.slice(0, -3)
   console.log(starttime);
   $('#StartTime').val(starttime);
   $('#EndTIme').val(endtime);
  $('#StartTime').datetimepicker({
                       format: 'HH:mm',
                              pickDate: false,
                              pickSeconds: false,
                              pick12HourFormat: false
                    });
                    $('#EndTIme').datetimepicker({
                        format: 'HH:mm',
                               pickDate: false,
                               pickSeconds: false,
                               pick12HourFormat: false

                    });
   $('#tr_BayLocationBayId').hide();
   $('#tr_BayLocation').hide();
   var hostType = $('#MasterOrPlayer').val();
   var hostMasterName = $("#HostMasterName").val();
   console.log($('#MasterOrPlayer').val());
    if(hostType == 'M') {
            $('#MasterOrPlayer').val(hostType);
         }
    $(document).on("change","#MasterOrPlayer", function (e) {
        if($("#MasterOrPlayer").val() == '{{ \Config::get('vyoma-constants.playerType') }}')
        {
          updateMasterName($('#BayId').val(), false);
        }
        else {
          $('#HostMasterName').prop('readonly', false);
          $("#HostMasterName").val(hostMasterName);
        }

     });

     function updateMasterName(bayId, setselected) {
      console.log('update master name');
      //console.log(bayId);
            $.ajax({
              url: '/playerinfo/'+bayId,
              type: "GET",
              success: function (data) {
                var sn = data;
                console.log(data)
                  $("#HostMasterName").val(sn);
                  $('#HostMasterName').prop('readonly', true);
              }
            });
          }
     $(document).on("change","#StationName", function (e) {
          updateBayLocation($("#StationName").val(), $("#StationName").val());
      });

     $(document).on("change","#BayId", function (e) {
          updateMasterName($("#BayId").val(), false);
      });

    function updateBayLocation(stationId, setselected) {
            $.ajax({
              url: '/bayInfo/'+stationId,
              type: "GET",
              success: function (data) {
                var sn = data;
                //console.log(data)

              $('#BayId').empty();
              //$('#BayId').append($('<option>').text("select"));
              $.each(data, function(i, obj){
                console.log(setselected);
                console.log(obj.BayId);
                  if(setselected == obj.BayId) {
                    $('#BayId').append($('<option>').text(obj.BayLocationDesc).attr('value', $("#StationName").val()));
                  }
                  else {
                    $('#BayId').append($('<option>').text(obj.BayLocationDesc).attr('value', obj.BayId));
                  }
            });
          }
        });
    }


    $('#StationName option').formatToUpperCase();

      // Get the deafult selected values
      branchVal = $("#StationName").val();

      // Sort options in alphabetical order
      $("#StationName").sortOptions(branchVal);
}

  function addBefore(formId){
   $('#StartTime').val('00:00');
   $('#EndTIme').val('00:00');

   $('#StartTime').datetimepicker({
                        format: 'HH:mm',
                               pickDate: false,
                               pickSeconds: false,
                               pick12HourFormat: false

                     });
                     $('#EndTIme').datetimepicker({
                         format: 'HH:mm',
                                pickDate: false,
                                pickSeconds: false,
                                pick12HourFormat: false
                     });
   $('#tr_BayLocationBayId').hide();
   $('#MasterOrPlayer').prepend("<option value='' selected>Select</option>");
   $('#HostCounterType').prepend("<option value='' selected>Select</option>");
   $('#status').prepend("<option value='' selected>Select</option>");
  }

 </script>
    <table id="jqGridHost" class="table "></table>
    </div>
        {!!
        GridRender::setGridId("jqGridHost")
          ->enablefilterToolbar()
          ->setGridOption('url',$buildUrl)
          ->setGridOption('editurl',URL::to('/host/crud'.$redirect))
          ->setGridOption('rowNum', 200)
          ->setGridOption('rownumbers', true)
          ->setGridOption('autowidth', true)
          ->setGridOption('height', 500)
          ->setGridOption('caption','Host')
          ->setGridOption('viewrecords',true)
          ->setGridOption('postData',array('filters'=>"{'groupOp':'AND','rules':[{'field':'Host.status','op':'eq','data': $status }]}"))
          ->setGridOption('cmTemplate', array('resizable' => false))
          ->setGridEvent('gridComplete', 'hostFunctionAfterGridComplete')
          ->setGridEvent('loadComplete', 'dynamicHeight')

          ->setNavigatorOptions('navigator', array('add' => true, 'edit' => true, 'del' => false, 'view' => true, 'refresh' => false))
          ->setNavigatorOptions('add', array('closeAfterAdd' => true, 'addCaption' => 'Add Host '.$stationName, 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '80'))
          ->setNavigatorEvent('add', 'afterSubmit', 'afterSubmitEvent')
          ->setNavigatorOptions('edit', array('closeAfterEdit' => true, 'editCaption' => 'Edit Host Details', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '80'))
          ->setNavigatorEvent('edit', 'afterSubmit', 'afterSubmitEvent')
          ->setNavigatorOptions('view', array('closeAfterEdit' => true, 'caption' => 'View Host Info', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '80'))
          ->setFileProperty('FileName', 'Hosts')
          ->addColumn(array('index' => 'HostId', 'hidden' => true, 'editable' => true, 'key' => true))
          ->addColumn(array('label' => 'Host Type','index' => 'MasterOrPlayer', 'editable' => true, 'editrules' => array('required' => true), 'edittype'=>'select','editoptions' => array( 'value' => 'M:Master;P:Player;B:Both'), 'align' => 'left', 'width' => '105px', 'search' => true, 'stype'=> 'select', 'searchoptions' => array('sopt'=> ['eq', 'ne'], 'value' => ":All;M:Master;P:Player;B:Both")))
          ->addColumn(array('label' => 'Station', 'index'=>'StationName', 'width' => '110px', 'editable' => true, 'edittype' => $editTypeStation,'editoptions' => array( 'value' => $DetailsOfStation, 'readonly' => 'readonly'), 'align' => 'left',))

          ->addColumn(array('label' => 'Bay Location', 'index' => 'BayLocationDesc', 'hidden' => false,'editable' => false, 'viewable' => false, 'align' => 'left', 'editrules' => array('edithidden' => true)))

          ->addColumn(array('label' => 'Bay Location', 'index' => 'BayId', 'hidden' => true,'editable' => true, 'viewable' => true, 'editrules' => array('edithidden' => true), 'edittype' => $editTypeBayLocation,'editoptions' => array( 'value' => $listOfBays, 'readonly' => 'readonly'), 'align' => 'left'))

          ->addColumn(array('label' => 'BayLocationBayId', 'index' => 'BayLocationBayId', 'hidden' => true,'editable' => true, 'viewable' => false, 'editrules' => array('edithidden' => true), 'edittype' => $editTypeBayLocation,'editoptions' => array( 'value' => $bayID, 'readonly' => 'readonly'), 'align' => 'left'))

          ->addColumn(array('label' => 'Bay Location', 'index' => 'BayLocation', 'hidden' => true,'editable' => $editable, 'viewable' => false, 'editrules' => array('edithidden' => true), 'edittype' => $editTypeBayLocation,'editoptions' => array( 'value' => $listOfBays, 'readonly' => 'readonly'), 'align' => 'left'))


          ->addColumn(array('label' => 'Host Name', 'index'=>'HostName', 'editable' => true, 'editrules' => array('required' => true), 'width' => '110px', 'align' => 'left',))
          ->addColumn(array('label' => 'Master Name','index' => 'HostMasterName', 'editable' => true, 'editrules' => array('required' => true), 'width' => '100px', 'align' => 'left'))
          ->addColumn(array('label' => 'Counter Type','index' => 'HostCounterType', 'editable' => true, 'editrules' => array('required' => true), 'edittype'=>'select','editoptions' => array( 'value' => 'PRS:PRS;UTS:UTS'), 'align' => 'left','width' => '90px', 'stype'=> 'select', 'searchoptions' => array('sopt'=> ['eq', 'ne'], 'value' => ":All;PRS:PRS;UTS:UTS")))
          ->addColumn(array('label' => 'Counter Number','index' => 'CounterNumber', 'editable' => true, 'editrules' => array('required' => true),'align' => 'center', 'width' => '105px'))
          ->addColumn(array('label' => 'Branch', 'index'=>'BranchName', 'align' => 'left', 'width' => '90px'))
          ->addColumn(array('label' => 'Region', 'index'=>'RegionName', 'align' => 'left','width' => '90px'))
          ->addColumn(array('label' => 'Start Time','index' => 'StartTime','hidden' => true, 'editable' => true, 'editrules' => array('required' => true,'align' => 'left', 'edithidden' => true)))
          ->addColumn(array('label' => 'End TIme','index' => 'EndTIme', 'hidden' => true, 'editable' => true, 'align' => 'left', 'editrules' => array('required' => true, 'edithidden' => true)))
          ->addColumn(array('label' => 'Status','index' => 'Host.status', 'hidden' => false, 'align' => 'left','edittype'=>'select','editoptions' => array( 'value' => 'A:Active;I:Inactive'),'editable' => true, 'editrules' => array('required' => true, 'edithidden' => false), 'width' => '90px', 'formatter' => 'statusDisplay', 'stype'=> 'select', 'searchoptions' => array('sopt'=> ['eq', 'ne'], 'value' => $searchOption)))

          ->setNavigatorEvent('edit','beforeShowForm', 'editBefore')

          ->setNavigatorEvent('add','beforeShowForm', 'addBefore')
          ->setNavigatorEvent('edit','onInitializeForm', 'intializeBefore')
          ->setNavigatorEvent('view','beforeShowForm', 'viewBaylocationInfo')
          ->renderGrid();
        !!}
 </div>

@stop
