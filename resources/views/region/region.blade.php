 <style>
.ui-jqgrid .ui-search-table{
  width: 300px !important;
  float: right;
}
</style>
@extends('layouts.apptemplate')

@section('title')
    Region
@endsection

@section('menu')
@include('layouts.menu')
@endsection

@section('content')
@include('layouts.validationAlert')
<div class="">
    <div class="">
        <table id="jqGridRegion" class="table "></table>
    </div>
        {!!
        GridRender::setGridId("jqGridRegion")
          ->enablefilterToolbar()
          ->setGridOption('url',URL::to('/region-grid'))
          ->setGridOption('editurl',URL::to('/region/crud'))
          ->setGridOption('rowNum', 20)
          ->setGridOption('rownumbers', true)
          ->setGridOption('autowidth', true)
          ->setGridOption('height', 500)
          ->setGridOption('caption','Region')
          ->setGridOption('viewrecords',true)
          ->setGridOption('cmTemplate', array('resizable' => false))
          ->setGridEvent('gridComplete', 'gridWithAutoSearch')
          ->setGridEvent('loadComplete', 'dynamicHeightBR')

          ->setNavigatorOptions('navigator', array('add' => true, 'edit' => true, 'del' => false, 'view' => true,'refresh' => false))
          ->setNavigatorOptions('add', array('closeAfterAdd' => true, 'addCaption' => 'Add Region', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '193'))
          ->setNavigatorEvent('add', 'afterSubmit', 'afterSubmitEvent')
          ->setNavigatorOptions('edit', array('closeAfterEdit' => true, 'editCaption' => 'Edit Region Details', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '80'))
          ->setNavigatorEvent('edit', 'afterSubmit', 'afterSubmitEvent')
          ->setNavigatorOptions('view', array('closeAfterEdit' => true, 'caption' => 'View Region Info', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '193'))
          ->setNavigatorOptions('del', array('closeAfterEdit' => true, 'caption' => 'Delete Region Info', 'resize' => false, 'drag' => false, 'left' => '502', 'top' => '188'))
          ->setNavigatorEvent('del', 'afterSubmit', 'afterSubmitEvent')
          ->setFileProperty('FileName', 'Regions')
          ->addColumn(array('index' => 'RegionId', 'hidden' => true, 'editable' => true, 'key' => true))
          ->addColumn(array('label' => 'Region Name', 'index'=>'RegionName', 'editable' => true, 'align' => 'left','editrules' => array('required' => true),
             'formatter' => 'showlink',
             'formatoptions' => array('baseLinkUrl'=>'/station', 'idName' => 'region')))
          ->addColumn(array('label' => 'Status','index' => 'status', 'hidden' => false, 'edittype'=>'select', 'align' => 'left','editoptions' => array( 'value' => 'A:Active;I:Inactive'),'editable' => true, 'editrules' => array('required' => true, 'edithidden' => false), 'width' => '90px', 'formatter' => 'statusDisplay', 'stype'=> 'select', 'searchoptions' => array('sopt'=> ['eq', 'ne'], 'value' => ":All;A:Active;I:Inactive")))
          ->renderGrid();
        !!}
  </div>



@stop
