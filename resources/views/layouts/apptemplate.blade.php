
<!DOCTYPE html>
    <head>
        <title>Vyoma-CRUD ::@yield('title')</title>

        <link rel="icon" href="/assets/crud-app/css/uibootstrap/images/favicon.ico" />

        @yield('css')

        <link rel="stylesheet" type="text/css"  href="/assets/crud-app/css/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css"  href="/assets/bootstrap-v3.2.0/css/bootstrap-theme.css"/>
        <link rel="stylesheet" type="text/css"  href="/assets/bootstrap-v3.2.0/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css"  href="/assets/crud-app/css/uibootstrap/jquery-ui-1.10.0.bootstrap.css" />
        <link rel="stylesheet" type="text/css"  href="/assets/crud-app/css/trirand/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css"  href="/assets/crud-app/css/trirand/ui.jqgrid-bootstarp.css" />
        <link rel="stylesheet" type="text/css"  href="/assets/crud-app/css/font-awesome.css" />
        <link rel="stylesheet" type="text/css"  href="/assets/crud-app/css/app.css" />
        <link rel="stylesheet" type="text/css"  href="/assets/crud-app-custom-ui/dashboard_styles.css" />
        <link rel="stylesheet" type="text/css"  href="/assets/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.css" />

	    <script type="text/javascript" src="/assets/crud-app/js/jquery.min.js"></script>
        <script type="text/javascript" src="/assets/crud-app/js/trirand/jquery.jqGrid.js"></script>
        <script type="text/javascript" src="/assets/crud-app/js/trirand/i18n/grid.locale-en.js"></script>
	    <script type="text/javascript" src="/assets/bootstrap-v3.2.0/js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="/assets/crud-app/js/main.js"></script>
      <script type="text/javascript" src="/assets/crud-app/js/jquery.autocomplete-1.4.2.js"></script>
        <script type="text/javascript" src="/assets/crud-app/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/assets/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.js"></script>
          <script type="text/javascript" src="/assets/crud-app/css/uibootstrap/moment.min.js"></script>

              <script type="text/javascript" src="/assets/crud-app/css/uibootstrap/bootstrap-datetimepicker.min.js"></script>
    </head>
    <body>
        @yield('menu')

        <div class='grid_holder'>
            @include('layouts.notifications')
            @yield('content')
	    </div>
    </body>
</html>
