@extends('layouts.apptemplate')

@section('title')
    Bay
@endsection

@section('menu')
@include('layouts.menu')
@endsection

@section('content')
@include('layouts.validationAlert')
<div class="">
<script>
$(document).ready(function(){
  // Prevent Actions Column Export
  bayAddHostExportFix();
});

function editSelectForDropdown() {
        // Convert to uppercase case
        $('#StationName option').formatToUpperCase();
        $('#BayLocationDesc option').formatToUpperCase();
        $('#App option').formatToUpperCase();

        // Get the deafult selected values
        branchVal = $("#StationName").val();
        regionVal = $("#BayLocationDesc").val();
        stateVal = $("#App").val();

        // Sort options in alphabetical order
        $("#StationName").sortOptions(branchVal);
        $("#BayLocationDesc").sortOptions(regionVal);
        $("#App").sortOptions(stateVal);
      }


function addBefore(formId){
    //Sorting for Station name //
    $('#StationName').append($("#StationName option").remove().sort(function(a, b) {
        var getFirstValueText = $(a).text(), getSecondValueText = $(b).text();
        return (getFirstValueText > getSecondValueText)?1:((getFirstValueText < getSecondValueText)?-1:0);
    })).prepend("<option value='' selected>Select</option>");
    // Sorting for Bay Location //
    $('#BayLocationDesc').append($("#BayLocationDesc option").remove().sort(function(a, b) {
        var getFirstValueText = $(a).text(), getSecondValueText = $(b).text();
        return (getFirstValueText > getSecondValueText)?1:((getFirstValueText < getSecondValueText)?-1:0);
    })).prepend("<option value='' selected>Select</option>");
    $('#App').prepend("<option value='' selected>Select</option>");
}


      var myfilter = { groupOp: "AND", rules: []};
      var ftype ='' , fvalue = '', url = '';
      @if(isset($status))
        ftype = 'Bay.status';
        fvalue = '{{$status}}';
        myfilter.rules.push({field:ftype,op:"eq",data:fvalue});
      @endif
</script>
    <div class="">
        <table id="jqGridBay" class="table "></table>
    </div>
      {!!
        GridRender::setGridId("jqGridBay")
          ->enablefilterToolbar()
          ->setGridOption('url',$buildUrl)
          ->setGridOption('editurl',URL::to('/bay/crud'))
          ->setGridOption('rowNum', 50)
          ->setGridOption('rownumbers', true)
          ->setGridOption('autowidth', true)
          ->setGridOption('height', 500)
          ->setGridOption('caption','Bay')
          ->setGridOption('viewrecords',true)
          ->setGridOption('postData',array('filters'=>"{'groupOp':'AND','rules':[{'field':'Bay.status','op':'eq','data': $status }]}"))
          ->setGridOption('cmTemplate', array('resizable' => false))
          ->setGridEvent('gridComplete', 'addButtonHostEvent')
          ->setGridEvent('loadComplete', 'disableInactiveAddHosts')

          ->setNavigatorOptions('navigator', array('add' => true, 'edit' => true, 'del' => false, 'view' => true, 'refresh' => false))
          ->setNavigatorOptions('add', array('closeAfterAdd' => true, 'addCaption' => 'Add Bay', 'resize' => false, 'drag' => false, 'left' => '387', 'top' => '193'))
          ->setNavigatorEvent('add', 'afterSubmit', 'afterSubmitEvent')
          ->setNavigatorOptions('edit', array('closeAfterEdit' => true, 'editCaption' => 'Edit Bay Details', 'resize' => false, 'drag' => false, 'left' => '387', 'top' => '193'))
          ->setNavigatorEvent('edit', 'afterSubmit', 'afterSubmitEvent')
          ->setNavigatorOptions('view', array('closeAfterEdit' => true, 'caption' => 'View Bay Info', 'resize' => false, 'drag' => false, 'left' => '387', 'top' => '193'))
          ->setNavigatorOptions('del', array('closeAfterEdit' => true, 'caption' => 'Delete Bay Info', 'resize' => false, 'drag' => false, 'left' => '502', 'top' => '223'))
          ->setNavigatorEvent('del', 'afterSubmit', 'afterSubmitEvent')

          ->setFileProperty('FileName', 'Bays')
          ->addColumn(array('index' => 'BayId', 'hidden' => true, 'editable' => true, 'key' => true))
          ->addColumn(array('label' => 'Station Name', 'index'=>'StationName', 'editable' => true, 'editrules' => array('required' => true), 'edittype'=>'select','align' => 'left','editoptions' => array( 'value' => $listOfStations )))
          ->addColumn(array('label' => 'Bay Location Name','index' => 'BayLocationDesc', 'align' => 'left','editable' => true,'edittype'=>'select','editoptions' => array( 'value' => $listOfBayLocations )))
          ->addColumn(array('label' => 'App','index' => 'App', 'editable' => true, 'editrules' => array('required' => true), 'align' => 'left', 'edittype'=>'select','editoptions' => array( 'value' => 'DDIS:DDIS; VMC:VMC')))
          ->addColumn(array('label' => 'Status','index' => 'Bay.status', 'hidden' => false, 'edittype'=>'select', 'align' => 'left','editoptions' => array( 'value' => 'A:Active;I:Inactive'),'editable' => true, 'editrules' => array('required' => true, 'edithidden' => false), 'width' => '90px', 'formatter' => 'statusDisplay', 'stype'=> 'select', 'searchoptions' => array('sopt'=> ['eq', 'ne'], 'value' => $searchOption)))
          ->addColumn(array('label' => 'Actions', 'name'=>'addHost', 'sortable' => false,'editable' => false,
            'search'=>false, 'formatter' => 'showlink',
             'formatoptions' => array('baseLinkUrl'=>'/host', 'addParam' => '/redirect',
             'idName' => 'bay')))
          ->setNavigatorEvent('add','beforeShowForm', 'addBefore')
          ->setNavigatorEvent('edit','beforeShowForm', 'editSelectForDropdown')
          ->renderGrid();
        !!}
 </div>
@stop
