<?php
return [

    /*
    * Constant value for Master, Player type in Host Crud
    */

    'masterType' => 'M',

    'playerType' => 'P',

    'bothType' => 'B',

    'noOfsuggestions' => 5,

    'status' => array('Active'=> 'A', 'Inactive'=>'I', 'Delete' => 'D')
];
