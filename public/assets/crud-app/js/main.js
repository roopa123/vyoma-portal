/**
 * Created by Bharathy  on 04-01-2016.
 */

  $(document).ready(function() {

    $("#jqGridBranches, #jqGridHost, #jqGridBay, #jqGridBayLocation, #jqGridStation, #jqGridRegion").bind("jqGridAddEditErrorTextFormat",
      function (data, response) {
      $errors = response.responseJSON.errors;
      $.each( $errors, function( key, value ) {
           errorTextFormat(value); //showing only the first error.
           return false;
      });
      // Flash Messages
      $("#success-alert").fadeTo(1000, 500).slideUp(500, function(){
        $("#success-alert").alert('close');
      });

    });

    /****** branch grid ******/
    var branchTopPager = document.getElementById( "jqGridBranchesPager" );
    $('#gview_jqGridBranches .ui-jqgrid-titlebar').next().prepend(branchTopPager);

    /****** region grid ******/
    var regionTopPager = document.getElementById( "jqGridRegionPager" );
    $('#gview_jqGridRegion .ui-jqgrid-titlebar').next().prepend(regionTopPager);

    /****** station grid ******/
    var stationTopPager = document.getElementById( "jqGridStationPager" );
    $('#gview_jqGridStation .ui-jqgrid-titlebar').next().prepend(stationTopPager);

    /****** bay-location grid ******/
    var bayLocTopPager = document.getElementById( "jqGridBayLocationPager" );
    $('#gview_jqGridBayLocation .ui-jqgrid-titlebar').next().prepend(bayLocTopPager);

    /****** branch grid ******/
    var bayTopPager = document.getElementById( "jqGridBayPager" );
    $('#gview_jqGridBay .ui-jqgrid-titlebar').next().prepend(bayTopPager);

    /****** hosts grid ******/
    var hostsTopPager = document.getElementById( "jqGridHostPager" );
    $('#gview_jqGridHost .ui-jqgrid-titlebar').next().prepend(hostsTopPager);

    // MAKE POPUP AS STATIC DURING ADD-HOST EVENT
    $.jqm.params.closeoverlay = false;
  });

 $(document).on("change","#MasterOrPlayer", function (e) {
    var urlParameter = window.location.pathname;
    var vars = urlParameter.split("/");
    var Id;
    id = '#BayId';
    if (vars[2] == 'bay'){
       id = '#BayLocationBayId';
    }
    if($("#MasterOrPlayer").val() == 'P')
    {
      updateMasterName($(id).val(), false);
    } else {
      $('#HostMasterName').prop('readonly', false);
      $('#HostMasterName').val('');
    }
  });

  function afterSubmitEvent(response, postdata)
  {
    //Parses the JSON response that comes from the server.
    result = JSON.parse(response.responseText);
    //result.success is a boolean value, if true the process continues,
    //if false an error message appear and all other processing is stopped,
    //result.message is ignored if result.success is true.
    if(result.status === 422) {
      $errors = result.errors;
      //errorTextFormat($errors);
      $.each( $errors, function( key, value ) {
           return value;
      });
    } else {
        errorsHtml = '<div class="alert alert-success" style="display: block"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><ul>';
        errorsHtml += '<li>' + result.message + '</li>'; //showing only the first error.
        errorsHtml += '</ul></div>';

        $( '#form-success' ).html( errorsHtml );
        console.log(result.redirect);
        if(result.redirect == true)
        {
          console.log(result.redirect);
          window.location="/bay";
          return [result.success, result.message];
        }
      setTimeout(function(){ $('.alert-success').fadeOut(1500) }, 5000);

      return [result.success, result.message];
    }

  }

  function updateMasterName(bayId, setselected) {
    console.log(bayId)
    $.ajax({

      url: '/playerinfo/'+bayId,
      type: "GET",
      success: function (data) {
        var sn = data;
        console.log(data)
          $("#HostMasterName").val(sn);
          $('#HostMasterName').prop('readonly', true);

      }
    });
  }

  function findAllStationByBranch(cellvalue, options, rowObject) {
    return "<a href='station/branch/"+rowObject.BranchId+"'>"+cellvalue+"</a>";
  }

  function linksToRegion(cellvalue, options, rowObject) {
    return "<a href='station/region/"+rowObject.regId_sum+"'>"+cellvalue+"</a>";
  }

  function homeGridComplete() {
    var x = $('.jqGridHomeghead_0');
    var branchName, branchValue, domMani, spanEle;
     x.each(function(index,value) {
      domMani = $(value).find('td:first');
      spanEle = $(domMani).html();
      branchName = domMani.text();
      spanEle = spanEle.replace(branchName, '');
      branchValue = $(value).next().find('td[aria-describedby="jqGridHome_branchId_sum"]').text();
      domMani.html(spanEle+' <a href="station/branch/'+branchValue+'">'+branchName+'</a>');
      });

    pivotHomeTableCalculate();
    pivotHomeTableRowSpan();
    dynamicHeight(true);
  }

  function linksToBranch(cellvalue, options, rowObject) {
    return cellvalue;
  }

  function noOfStations(cellvalue, options, rowObject){
    return '<a href="/station/regionHome/'+rowObject.regId_sum+'">'+rowObject.stations_sum+'</a>';

  }

  function noOfBays(cellvalue, options, rowObject){
    return '<a href="/bay/region/'+rowObject.regId_sum+'/status/A">'+rowObject.bay_sum+'</a>';

  }

  function noOfHosts(cellvalue, options, rowObject){
    return '<a href="/host/region/'+rowObject.regId_sum+'/status/A">'+rowObject.host_sum+'</a>';

  }


  function statusDisplay(cellvalue, options, rowObject){
    console.log(cellvalue);
    if(cellvalue == 'A') {
      cellvalue = 'Active';
    }
    else if(cellvalue == 'I'){
      cellvalue = 'Inactive';
    }
    else if(cellvalue == 'D'){
      cellvalue = 'Delete';
    }
    return cellvalue;
  }

  var errorTextFormat = function (response) {
    $('.FormError.ui-state-error').text(response).show();
  }

  var gridCompleteEvent = function () {
      $('.ui-search-input input').attr("placeholder", "Search");
      $('grid_selector').trigger("reloadGrid");
      $("#jqGridBranchesXlsButton").attr("title", "Download Xls");
      $("#jqGridBranchesCsvButton").attr("title", "Download Csv");
      $("#jqGridRegionXlsButton").attr("title", "Download Xls");
      $("#jqGridRegionCsvButton").attr("title", "Download Csv");
      $("#jqGridStationXlsButton").attr("title", "Download Xls");
      $("#jqGridStationCsvButton").attr("title", "Download Csv");
      $("#jqGridBayLocationXlsButton").attr("title", "Download Xls");
      $("#jqGridBayLocationCsvButton").attr("title", "Download Csv");
      $("#jqGridBayXlsButton").attr("title", "Download Xls");
      $("#jqGridBayCsvButton").attr("title", "Download Csv");
      $("#jqGridHostXlsButton").attr("title", "Download Xls");
      $("#jqGridHostCsvButton").attr("title", "Download Csv");

      $('.ui-icon.ui-icon-pin-s').attr("title", "Pin Grid");
      $(".ui-icon.ui-icon-refresh").attr("title", "Refresh Grid");
  }

  var refreshPagewithcount = function() {
    gridCompleteEvent();
    searchAutoComplete('#gs_BranchName', 'jqGridBranches');
  }


  var gridWithAutoSearch = function() {
    gridCompleteEvent();
    searchAutoComplete('#gs_RegionName', 'jqGridRegion');
  }

  var stationAutoComplete = function() {
    gridCompleteEvent();
    searchAutoComplete('#gs_StationName', 'jqGridStation');
    searchAutoComplete('#gs_BranchName', 'jqGridStation');
    searchAutoComplete('#gs_RegionName', 'jqGridStation');
    searchAutoComplete('#gs_StateName', 'jqGridStation');
  }


  var readOnlyColumn = function() {
    gridCompleteEvent();
    searchAutoComplete('#gs_BayLocationCode', 'jqGridBayLocation');
    searchAutoComplete('#gs_BayLocationDesc', 'jqGridBayLocation');
  }

  var addButtonHostEvent = function()  {
    gridCompleteEvent();
    var gridObj, ids;
    gridObj = jQuery('#jqGridBay');
    ids = jQuery('#jqGridBay').jqGrid('getDataIDs');
    console.log(ids);
    for(var i=0;i < ids.length;i++){
      var cl = ids[i];
      be = "<input class='add_cell_btn' style='height:30px;width:100px;' type='button' value='Add Host' onclick=\"jQuery('#add_jqGridHost').saveRow('"+cl+"');\"  />";
      gridObj.jqGrid('setRowData',ids[i],{addHost:be});
    }

    $('.ui-search-input #gs_actions').hide();
    $('.ui-search-clear .clearsearchclass').hide();

    searchAutoComplete('#gs_StationName', 'jqGridBay');
    searchAutoComplete('#gs_BayLocationDesc', 'jqGridBay');
    searchAutoComplete('#gs_App', 'jqGridBay');
  }

  function addHomeRegionLink (cellvalue, options, rowObject) {
    return '<a href="/station/region/'+rowObject.regId+'">'+rowObject.regName+'</a>';
  }

  var hostFunctionAfterGridComplete = function() {
    gridCompleteEvent();

     $('.ui-icon-plus').hide();
     $('.ui-icon-trash').hide();
     $('#tr_BayId').hide();
     console.log("m in host");
    var stationNameObj, bayLocationObj,locationid, stationNameVal, bayLocationVal, locationval, urlParameter = window.location.pathname;
    console.log(urlParameter);
    var vars = urlParameter.split("/");
    console.log(vars);
    if (vars[2] == 'bay')
      $('.ui-icon-plus').click();
      $('#tr_BayId').hide();
      stationNameObj = $('#StationName');
      stationNameVal = stationNameObj.attr('value');
      stationNameObj.val(stationNameVal);

      bayLocationObj = $('#BayLocation');
      bayLocationVal = bayLocationObj.attr('value');
      bayLocationObj.val(bayLocationVal);

      locationid = $('#BayLocationBayId');
      locationval = locationid.attr('value');
      locationid.val(locationval);

      $('#cData').click(function(){
          window.location='/bay';
      });
      if (vars[1]== 'host' && vars[2] == 'bay') {
        $('.ui-jqdialog-titlebar-close').click(function(){
            window.location='/bay';
        });
      }

    searchAutoComplete('#gs_StationName', 'jqGridHost');
    searchAutoComplete('#gs_BayLocationDesc', 'jqGridHost');
    searchAutoComplete('#gs_HostName', 'jqGridHost');
    searchAutoComplete('#gs_HostMasterName', 'jqGridHost');
    searchAutoComplete('#gs_HostCounterType', 'jqGridHost');
    searchAutoComplete('#gs_CounterNumber', 'jqGridHost');
    searchAutoComplete('#gs_BranchName', 'jqGridHost');
    searchAutoComplete('#gs_RegionName', 'jqGridHost');
  }

  var afterShowFormFunction = function() {
    console.log('form after show');
  }

/* Make the Actions column as
 * hidden in jQgrid colmodel object
 * and prevent from export
**/
function bayAddHostExportFix() {
  $("#jqGridBayXlsButton, #jqGridBayCsvButton").hover(
    function() {
      colmodelData = jQuery("#jqGridBay").getGridParam("colModel");
      console.log(colmodelData[5].value);
      colmodelData[6].hidden=true;
    }, function() {
      colmodelData = jQuery("#jqGridBay").getGridParam("colModel");
      colmodelData[6].hidden=false;
    }
  );
}


//autocomplete for search
 function searchAutoComplete(colName, gridName)
{
  console.log(gridName);
  var sourceUrl, col;
  if(gridName === "jqGridBranches") {
      col = colName.substring(4);
      sourceUrl = "/search/branchAutocomplete/"+col;
    }
    else if(gridName === 'jqGridRegion'){
      col = colName.substring(4);
      sourceUrl = "/search/regionAutocomplete/"+col;
    }
    else if(gridName === 'jqGridStation'){
      col = colName.substring(4);
      sourceUrl = "/search/stationAutocomplete/"+col;
    }
    else if(gridName === 'jqGridBayLocation'){
      col = colName.substring(4);
      sourceUrl = "/search/bayLocationAutocomplete/"+col;
    }
    else if(gridName === 'jqGridBay'){
      col = colName.substring(4);
      sourceUrl = "/search/bayAutocomplete/"+col;
    }
    else if(gridName === 'jqGridHost'){
      col = colName.substring(4);
      sourceUrl = "/search/hostAutocomplete/"+col;
    }
    console.log(sourceUrl);
   $(colName).autocomplete({

    source: sourceUrl,
    minLength: 2,
    select: function(event, ui) {
      $(colName).val(ui.item.value);
      console.log('teste');

      var e = jQuery.Event("keypress");
      e.which = 13; // # Some key code value
      e.keyCode = 13;
      $(colName).trigger(e);
    }
  });
}

var disableInactiveAddHosts = function () {
  var row = $('.jqgrow');
  $.each(row, function(index,value){
    txt = $('> td:eq(5)', this).text();
    if(txt == 'Inactive') {
      $(this).parent();
      $('> td:eq(6) > a > input', this).attr('disabled', true).css('color', 'red');
    }
  });
  dynamicHeight();
}

// HOME DASHBOARD CALCULATE TOTAL COUNTS
function pivotHomeTableCalculate() {
  // List of columns for count operation
  var columns = [4, 5, 6];

  // Iterate through the columns list
  $.each(columns, function(index, value) {
    var countArr = [], count = 0; loopCount = 0;
    // Iterate through the grid table rows
    $("#jqGridHome tr").each(function() {
      loopCount++;
      // Skip the first two empty rows
      if(loopCount < 3) return true;
      content = $(this).find('td:eq('+value+')').text();
      // If digits
      if(content.match(/^\d+$/) != null) {
        count = count + parseInt(content);
      }
      // If non-digits
      if (content.match(/^\d+$/) == null) {
        // Push to array
        countArr.push(count);
        // clear count
        count = 0;
      }
    });

    // Push the last count to array
    countArr.push(count);
    // Clear loopCount
    loopCount = 0;

    // Put the collected(array) data to the grid
    $("#jqGridHome tr.jqgroup").each(function () {
    $(this).find('td:eq('+value+')').text('Total Count : ' + countArr[loopCount]).css({'text-align':'center','font-weight':'normal','color':'#156F86'});
    loopCount++;
    });
  });
// Regions count
homeGridRegionCount();
}

function homeGridRegionCount() {
    var countArr = [], count = 0; loopCount = 0;
    // Iterate through the grid table rows
    $("#jqGridHome tr").each(function() {
      loopCount++;
      // Skip the first two empty rows
      if(loopCount < 3) return true;
      content = $(this).find('td:eq(1)').text();
      // If content of td is not null
      if(content.length > 2) {
        count++
      }
      if (content.length < 2) {
        // Push to array
        countArr.push(count);
        // clear count
        count = 0;
      }
    });

    // Push the last count to array
    countArr.push(count);
    // Clear loopCount
    loopCount = 0;

    // Put the collected(array) data to the grid
    $("#jqGridHome tr.jqgroup").each(function () {
    $(this).find('td:eq(1)').text('Total Count : ' + countArr[loopCount]).css({'text-align':'center','font-weight':'normal','color':'#156F86'});
    loopCount++;
    });
}

// HOME DASHBOARD PIVOT TABLE ROWSPAN
function pivotHomeTableRowSpan() {
  $("#jqGridHome tr").each(function(){
   cell = $(this).find("td:first");
  if(cell.text().length == 0)
   cell.css('border-bottom', 'none');
   x = cell.text();
   if(x.length > 0) {
    $(this).prev().find("td:first").css('border-bottom', '1px solid #449FB7');
   }
  });
  $('#jqGridHome tr:last').find("td:first").css('border-bottom', '1px solid #449FB7');
}

// SET JQGRID HEIGHT DYNAMICALLY
function dynamicHeight(home) {
  if (home == true) {
    bDivHeight = 127;
  } else {
    bDivHeight = 177;
  }
  var bdiv_height = $(window).height()- bDivHeight;
  var view_height = $(window).height() - 50;
  $('.ui-jqgrid-bdiv').css('height', bdiv_height+'px');
  $('.ui-jqgrid-view').css('height', view_height+'px');
}

// SET JQGRID HEIGHT DYNAMICALLY(Branch, Home and Region dashboards)
function dynamicHeightBR() {
  var bdiv_height = $(window).height()- 220;
  var view_height = $(window).height() - 90;
  $('.ui-jqgrid-bdiv').css('height', bdiv_height+'px');
  $('.ui-jqgrid-view').css('height', view_height+'px');
}

// Select Dropdown Option formatter
      $.fn.formatToUpperCase = function() {
        $(this).each(function() {
            string = $(this).text();
            formatString = string.charAt(0).toUpperCase() + string.slice(1);
            // alert(formatString);
            $(this).html(formatString);
        });
      }

      // Select Dropdown Option Sorter
      $.fn.sortOptions = function(defaultVal){
        $(this).each(function(){
          var op = $(this).children("option");
          op.sort(function(a, b) {
            return a.text > b.text ? 1 : -1;
          });
          return $(this).empty().append(op);
        });
        $(this).val(defaultVal);
      }
